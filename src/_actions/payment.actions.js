import { PAYMENT_INFORMATION_FOUND,PAYMENT_INFORMATION_NOTFOUND,ADD_PAYMENT,UPDATE_PAYMENT,CHANGE_DATA_PAYMENT} from "./types.js";
import { Service } from '../_services';
import { getIdUser } from "../_services/UserServices.js";
import { toast } from 'react-toastify';
export const paymentActions = {
    getInformationPayment,
    changeDataPayment,
    updateDataPayment,
    addPayment
};
function getInformationPayment(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'payment/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            if(response.data.data){
            dispatch(changeInformationPayment(response.data.data));
            }
            else{
                dispatch(noInformationPayment());
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(noInformationPayment());
        })
    };
}
function changeDataPayment(name,value){
    return dispatch=>{
        dispatch(dataPayment(name,value));
    }
}
function addPayment(payment){
    return dispatch => {
        let apiEndpoint = 'payment';
        let payload=payment;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            toast.success("your company unformations are added");
            dispatch(addNewPayment(payment));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
function updateDataPayment(payment,id){
    return dispatch => {
        let apiEndpoint = 'payment/'+id;
        let payload=payment;
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            toast.info("your company unformations are updated");
            dispatch(updatePayment(payment));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
export function changeInformationPayment(informations){
    return {
        type:PAYMENT_INFORMATION_FOUND,
        data:informations
}
}
export function noInformationPayment(){
    return {
        type:PAYMENT_INFORMATION_NOTFOUND,
}
}
export function dataPayment(name,value){
    return{
        type:CHANGE_DATA_PAYMENT,
        name:name,
        value:value
    }
}
export function addNewPayment(payment){
    return{
        type:ADD_PAYMENT,
        payment:payment
    }
}
export function updatePayment(payment){
    return{
        type:UPDATE_PAYMENT,
        payment:payment
    }
}