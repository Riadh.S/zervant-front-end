import { COMPANY_INFORMATION_FOUND,COMPANY_INFORMATION_NOTFOUND,ADD_COMPANY,UPDATE_COMPANY,CHANGE_DATA_COMPANY} from "./types.js";
import { Service } from '../_services';
import { getIdUser } from "../_services/UserServices.js";
import { toast } from 'react-toastify';
export const companyActions = {
    getInformationCompany,
    changeDataCompany,
    updateDataCompany,
    addCompany
};
function getInformationCompany(){
    const id=getIdUser();
    console.log(id);
    return dispatch => {
        let apiEndpoint = 'companies/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            if(response.data.data){
            dispatch(changeInformationCompany(response.data.data));
            }
            else{
                dispatch(noInformationCompany());
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(noInformationCompany());
        })
    };
}
function changeDataCompany(name,value){
    return dispatch=>{
        dispatch(dataCompany(name,value));
    }
}
function addCompany(company){
    return dispatch => {
        let apiEndpoint = 'companies';
        let payload=company;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            toast.success("your company unformations are dded");
            dispatch(addNewCompany(company));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
function updateDataCompany(company,id){
    return dispatch => {
        let apiEndpoint = 'companies/'+id;
        let payload=company;
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            toast.info("your company unformations are updated");
            dispatch(updateCompany(company));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
export function changeInformationCompany(informations){
    return {
        type:COMPANY_INFORMATION_FOUND,
        data:informations
}
}
export function noInformationCompany(){
    return {
        type:COMPANY_INFORMATION_NOTFOUND,
}
}
export function dataCompany(name,value){
    return{
        type:CHANGE_DATA_COMPANY,
        name:name,
        value:value
    }
}
export function addNewCompany(company){
    return{
        type:ADD_COMPANY,
        company:company
    }
}
export function updateCompany(company){
    return{
        type:UPDATE_COMPANY,
        company:company
    }
}