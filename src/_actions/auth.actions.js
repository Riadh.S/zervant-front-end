import { Service } from '../_services';
import { history } from '../_helpers';
import { LOGIN_SUCCESS,LOGOUT_SUCCESS,LOGIN_FAILED} from "./types.js"

export const authActions = {
    login,
    logout
};

function login(email, password){
    return dispatch => {
        let apiEndpoint = 'auths';
        let payload = {
            email: email,   
            password: password
        }
       Service.post(apiEndpoint, payload) 
       .then((response)=>{
       console.log(response);     
       if (response.status===200) {
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('auth', response.data.auth);
                dispatch(setUserDetails(response.data));
                history.push('/home');
            }
            if(response.status===201){
                dispatch(loginFailed(response.data.message));
            }
            if(response.status===202){
                dispatch(loginFailed(response.data.message));
            }
  
    })
    .catch((err)=>{
        dispatch(loginFailed("problem"));
        console.log(err);
    });
}
}
function logout(){
    return dispatch => {
        localStorage.removeItem('auth');
        localStorage.removeItem('token');
        dispatch(logoutUser());
        history.push('/login');
    }
}

export function setUserDetails(user){
    return{
        type: LOGIN_SUCCESS,
        auth: user.auth,
        token: user.token,
        userResponse:'' 
    }
}   

export function logoutUser(){
    return{
        type: LOGOUT_SUCCESS,
        auth: false,
        token: ''
        }
}
export function loginFailed(err){
    return{
        type: LOGIN_FAILED,
        auth: false,
        token: '',
        userResponse:err
    }
}