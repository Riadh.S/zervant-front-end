import { BILLSETTING_INFORMATION_FOUND,BILLSETTING_INFORMATION_NOTFOUND,ADD_BILLSETTING,UPDATE_BILLSETTING,CHANGE_CHECKBOX_DATA_BILLSETTING,CHANGE_DATA_BILLSETTING} from "./types.js";
import {Service} from '../_services/Service'
import { getIdUser } from '../_services/UserServices';
import { toast } from 'react-toastify';
export const billSettingActions={
    getInformationBillSetting,
    changeDataBillSetting,
    changeCheckBoxBillSetting,
    addBill,
    updateDataBill
}
function changeCheckBoxBillSetting(name,value){
    return dispatch=>{
        dispatch(dataBillCheckBox(name,value));
    }
}
function getInformationBillSetting(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'bill_setting/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            if(response.data.data){
            dispatch(changeInformationBillSetting(response.data.data));
            }
            else{
                dispatch(noInformationBillSetting());
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(noInformationBillSetting());
        })
    };
}
function addBill(bill){
    console.log(bill);
    return dispatch => {
        let apiEndpoint = 'bill_setting';
        let payload=bill;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
           toast.success("your invoice setting is added"); 
            dispatch(addNewBill(bill));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
function updateDataBill(payment,id){
    return dispatch => {
        let apiEndpoint = 'bill_setting/'+id;
        let payload=payment;
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            toast.info("your invoice setting is updating"); 
            dispatch(updatePayment(payment));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
function changeDataBillSetting(name,value){
    return dispatch=>{
        dispatch(dataBill(name,value));
    }
}
export function changeInformationBillSetting(informations){
    return {
        type:BILLSETTING_INFORMATION_FOUND,
        data:informations
}
}
export function noInformationBillSetting(){
    return {
        type:BILLSETTING_INFORMATION_NOTFOUND,
}
}
export function dataBill(name,value){
    return{
        type:CHANGE_DATA_BILLSETTING,
        name:name,
        value:value
    }
}
export function dataBillCheckBox(name,value){
    return{
        type:CHANGE_CHECKBOX_DATA_BILLSETTING,
        name:name,
        value:value
    }
}
export function addNewBill(payment){
    return{
        type:ADD_BILLSETTING,
        payment:payment
    }
}
export function updatePayment(payment){
    return{
        type:UPDATE_BILLSETTING,
        payment:payment
    }
}