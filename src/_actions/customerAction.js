import { Service } from "../_services";
import {
  FETECHED_ALL_CUSTOMERS,
  ADD_CUSTOMER,
  ORDER_CUSTOMERS,
  CHANGE_DATA_CUSTOMER,
  SEARCH_CUSTOMERS,
  DELETE_CUSTOMER,
  DETAIL_CUSTOMERS,
  PAGINATION_CUSTOMER,
  UPDATE_CUSTOMER,
  UPDATE_CUSTOMER_FAILED,
  CHANGE_VALUE_CUSTOMER,
  CHANGE_CHECKBOX_DATA_CUSTOMER,
  ADD_STAR_DELIVERY_METHOD
} from "./types";
import { getIdUser } from "../_services/UserServices";

export const customerActions = {
  getCustomers,
  orderCustomers,
  searchCustomer,
  detailCustomer,
  CustomerPagination,
  updateCustomer,
  changeValueCustomer,
  deleteCustomer,
  changeDataCustomer,
  addCustomer,
  changeCheckBoxCustomer,
  dataCustomerCheckBox,
  addStarDeliveryMethod
};
export function changeCheckBoxCustomer(name, value) {
  return dispatch => {
    dispatch(dataCustomerCheckBox(name, value));
  };
}
export function dataCustomerCheckBox(name, value) {
  return {
    type: CHANGE_CHECKBOX_DATA_CUSTOMER,
    payload: { name, value }
  };
}
export function addStarDeliveryMethod(value, label) {
  return {
    type: ADD_STAR_DELIVERY_METHOD,

    payload: { value, label }
  };
}
export function getCustomers() {
  console.log("customers action get customers");

  const _id = getIdUser();
  console.log("_id", _id);
  return dispatch => {
    let apiEndpoint = "clients/" + _id;
    console.log("apiEndpoint", apiEndpoint);
    Service.get(apiEndpoint)
      .then(response => {
        console.log("success", response);
        dispatch(changeCustomersList(response.data.data));
      })
      .catch(err => {
        console.log("y'a une Error", err);
      });
  };
}
function addCustomer(customer) {
  return dispatch => {
    let apiEndpoint = "customers";
    let payload = customer;
    Service.post(apiEndpoint, payload)
      .then(response => {
        console.log("success add", response);
        dispatch(addNewCustomer(customer));
      })
      .catch(err => {
        console.log("Error");
        console.log(err);
      });
  };
}
function orderCustomers(val) {
  return dispatch => {
    dispatch(orderCustomersList(val));
  };
}
function changeValueCustomer(name, value) {
  return dispatch => {
    dispatch(changeCustomer(name, value));
  };
}
function changeDataCustomer(name, value) {
  return dispatch => {
    dispatch(dataCustomer(name, value));
  };
}
function searchCustomer(name) {
  return dispatch => {
    dispatch(searchCustomersList(name));
  };
}
function CustomerPagination(nbrPage) {
  console.log(nbrPage);
  return dispatch => {
    dispatch(paginationCustomersList(nbrPage));
  };
}
function detailCustomer(customer) {
  return dispatch => {
    dispatch(detailCustomersList(customer));
  };
}
function updateCustomer(_id, customer) {
  return dispatch => {
    let apiEndpoint = "customers/" + _id;
    let payload = customer;
    console.log(payload);
    Service.put(apiEndpoint, payload)
      .then(response => {
        if (response.status === 200) {
          dispatch(putCustomer(customer));
        }
      })
      .catch(err => {
        console.log("Error");
        console.log(err);
        dispatch(putCustomerFailed());
      });
  };
}
function deleteCustomer(id) {
  return dispatch => {
    let apiEndpoint = "customers/" + id;
    Service.deleteDetail(apiEndpoint)
      .then(res => {
        if (res.status === 200) {
          dispatch(deleteOneCustomer(id));
        }
      })
      .catch(err => console.log(err));
  };
}
export function changeCustomersList(customer) {
  return {
    type: FETECHED_ALL_CUSTOMERS,
    payload: customer
  };
}
export function orderCustomersList(val) {
  return {
    type: ORDER_CUSTOMERS,
    payload: val
  };
}
export function searchCustomersList(name) {
  return {
    type: SEARCH_CUSTOMERS,
    payload: name
  };
}
export function detailCustomersList(customer) {
  return {
    type: DETAIL_CUSTOMERS,
    payload: customer
  };
}
export function paginationCustomersList(nbrPage) {
  return {
    type: PAGINATION_CUSTOMER,
    payload: nbrPage
  };
}
export function putCustomer(customer) {
  return {
    type: UPDATE_CUSTOMER,
    payload: customer
  };
}
export function putCustomerFailed() {
  return {
    type: UPDATE_CUSTOMER_FAILED
  };
}
export function changeCustomer(name, value) {
  return {
    type: CHANGE_VALUE_CUSTOMER,

    payload: { name, value }
  };
}
export function dataCustomer(name, value) {
  return {
    type: CHANGE_DATA_CUSTOMER,
    payload: { name, value }
  };
}
export function deleteOneCustomer(id) {
  return {
    type: DELETE_CUSTOMER,
    payload: id
  };
}
export function addNewCustomer(customer) {
  return {
    type: ADD_CUSTOMER,
    payload: customer
  };
}
/*export const getCustomers = () => {
  console.log("customers action get customers");
  const _id = getIdUser();
  return dispatch => {
    let apiEndpoint = "customers/" + _id;
    Service.get(apiEndpoint)
      .then(response => {
        console.log(response);
        dispatch(changeCustomersList(response.data.data));
      })
      .catch(err => {
        console.log("Error");
        console.log(err);
      });
  };
};
const addCustomer = customer => {
  return dispatch => {
    let apiEndpoint = "customers";
    let payload = customer;
    Service.post(apiEndpoint, payload)
      .then(response => {
        dispatch(addNewCustomer(customer));
      })
      .catch(err => {
        console.log("Error");
        console.log(err);
      });
  };
};
const orderCustomers = val => {
  return dispatch => {
    dispatch(orderCustomersList(val));
  };
};
const changeValueCustomer = ({ name, value }) => {
  return dispatch => {
    dispatch(changeCustomer(name, value));
  };
};
const changeDataCustomer = ({ name, value }) => {
  return dispatch => {
    dispatch(dataCustomer(name, value));
  };
};
const searchCustomer = lastName => {
  return dispatch => {
    dispatch(searchCustomersList(lastName));
  };
};
const CustomerPagination = nbrPage => {
  console.log(nbrPage);
  return dispatch => {
    dispatch(paginationCustomersList(nbrPage));
  };
};
const detailCustomer = customer => {
  return dispatch => {
    dispatch(detailCustomersList(customer));
  };
};
const updateCustomer = ({ _id, customer }) => {
  return dispatch => {
    let apiEndpoint = "customers/" + _id;
    let payload = customer;
    console.log(payload);
    Service.put(apiEndpoint, payload)
      .then(response => {
        if (response.status === 200) {
          dispatch(putCustomer(customer));
        }
      })
      .catch(err => {
        console.log("Error");
        console.log(err);
        dispatch(putCustomerFailed());
      });
  };
};
const deleteCustomer = id => {
  return dispatch => {
    let apiEndpoint = "customers/" + id;
    Service.deleteDetail(apiEndpoint)
      .then(res => {
        if (res.status === 200) {
          dispatch(deleteOneCustomer(id));
        }
      })
      .catch(err => console.log(err));
  };
};
export const changeCustomersList = customer => {
  return {
    type: FETECHED_ALL_CUSTOMERS,
    payload: customer
  };
};
export const orderCustomersList = val => {
  return {
    type: ORDER_CUSTOMERS,
    payload: val
  };
};
export const searchCustomersList = lastName => {
  return {
    type: SEARCH_CUSTOMERS,
    name: lastName
  };
};
export const detailCustomersList = customer => {
  return {
    type: DETAIL_CUSTOMERS,
    payload: customer
  };
};
export const paginationCustomersList = nbrPage => {
  return {
    type: PAGINATION_CUSTOMER,
    payload: nbrPage
  };
};
export const putCustomer = customer => {
  return {
    type: UPDATE_CUSTOMER,
    payload: customer
  };
};
export const putCustomerFailed = () => {
  return {
    type: UPDATE_CUSTOMER_FAILED
  };
};
export const changeCustomer = ({ lastName, value }) => {
  return {
    type: CHANGE_VALUE_CUSTOMER,
    name: lastName,
    payload: value
  };
};
export const dataCustomer = ({ lastName, value }) => {
  return {
    type: CHANGE_DATA_CUSTOMER,
    name: lastName,
    payload: value
  };
};
export const deleteOneCustomer = id => {
  return {
    type: DELETE_CUSTOMER,
    payload: id
  };
};
export const addNewCustomer = customer => {
  return {
    type: ADD_CUSTOMER,
    payload: customer
  };
};

/*export function changeCustomersList(Customers) {
  return {
    type: FETECHED_ALL_CUSTOMERS,
    payload: Customers
  };
}*/
