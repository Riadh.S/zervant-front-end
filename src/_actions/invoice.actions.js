import { Service } from '../_services/';
import {FETECHED_ALL_INVOICE,ID_PDF,CHANGE_DATE,ADD_INVOICE,ADD_ROW_PRODUCT,NEW_INVOICE,CHANGE_DATA_INVOICE_PRODUCTS,FETCH_START,ORDER_INVOICES,CHANGE_DATA_INVOICE,SEARCH_INVOICES,DELETE_INVOICE,DETAIL_INVOICES,PAGINATION_INVOICE,UPDATE_INVOICE,UPDATE_INVOICE_FAILED,CHANGE_VALUE_INVOICE, CHANGE_DATA_DEVISSETTING} from './types';
import { getIdUser } from '../_services/UserServices';
import { saveAs } from 'file-saver';
export const invoiceActions = {
    getInvoices,
    orderInvoices,
    searchInvoice,
    detailInvoice,
    invoicePagination,
    updateInvoice,
    changeValueInvoice,
    deleteInvoice,
    changeDataInvoice,
    addInvoice,
    generatePdf,
    getPDF,
    changeDataInvoiceProducts,
    addRowProduct,
    newInvoice,
    changeDate
};

function getInvoices(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'bills/'+id;
        dispatch(fetchStarted());
        Service.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            dispatch(invoicesList(response.data.data));
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function newInvoice(data){
    console.log(data);
    return dispatch=>{
        dispatch(addInvoiceData(data));
    }
}
function changeDate(date,val){
    return dispatch=>{
        dispatch(changeDateProduct(date,val));
    }
}
function addRowProduct(){
    return dispatch=>{
     dispatch(addRow());
}
}
function generatePdf(invoice){
    return dispatch=>{
    let apiEndpoint = 'bills/create-pdf';
        let payload=invoice;
        Service.post(apiEndpoint,payload)

        .then((res) =>{
        if(res.data.data){
            console.log(res.data.data);
          dispatch(getIdPdf(res.data.data)); 
        }
    })
   .catch((err)=>{
    console.log(err);
   });
}
}
function getPDF(){
    let apiEndpoint = 'bills/fetch-pdf';
    Service.getpdf(apiEndpoint)
    .then((res) => { 
        
        const pdfBlob = new Blob([res.data], { type: 'application/pdf' });
        console.log(pdfBlob);
        saveAs(pdfBlob, 'invoice.pdf');
    }
      
)
.catch((err)=>console.log(err));
}
function addInvoice(invoice){
    return dispatch => {
        let apiEndpoint = 'bills';
        let payload=invoice;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            dispatch(addNewInvoice(invoice));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    }
}
function changeDataInvoiceProducts(name,value,index,products){
    console.log(products);
    return dispatch=>{
        dispatch(changeInvoiceProducts(name,value,index,products));
    }
}
function orderInvoices(val){

    return dispatch=>{
        dispatch(orderInvoicesList(val));
    }
}
function changeValueInvoice(name,value){
    return dispatch=>{
        dispatch(changeInvoice(name,value));
    }
}
function changeDataInvoice(name,value){
    return dispatch=>{
        dispatch(dataInvoice(name,value));
    }
}
function searchInvoice(productName){
    return dispatch=>{
        dispatch(searchInvoicesList(productName));
    }
}
function invoicePagination(nbrPage){
    console.log(nbrPage);
    return dispatch=>{
        dispatch(paginationInvoicesList(nbrPage));
    }
}
function detailInvoice(invoice){
    return dispatch=>{
        dispatch(detailInvoicesList(invoice));
    }
}
function updateInvoice(id,product){
    return dispatch=>{
        let apiEndpoint = 'bills/'+id;
        let payload =product;
        console.log(payload);
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            if(response.status===200){
            dispatch(putInvoice(product));
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(putInvoiceFailed());
        })
    }
}
function deleteInvoice(id){
    return dispatch =>{
        let apiEndpoint = 'bills/'+id;
        Service.deleteDetail(apiEndpoint)
        .then((res)=>{
            if(res.status===200){
                dispatch(deleteOneInvoice(id));
            }
        })
        .catch((err)=>console.log(err))
    }
}
export function invoicesList(invoices){
    return{
        type: FETECHED_ALL_INVOICE,
        invoices: invoices
    }
}
/*export function noChangeInvoicesList(){
    return{
        type:INVOICE_NOT_FOUND
    }
}*/
export function orderInvoicesList(val){
    return {
        type :ORDER_INVOICES,
        valeur:val
    }
}
export function getIdPdf(invoice){
    return {
        type:ID_PDF,
        invoice:invoice
    }
}
export function changeDateProduct(date,val){
    return{
        type:CHANGE_DATE,
        date:date,
        val:val
    }
}
export function changeInvoiceProducts(name,value,index,products){
    return {
        type:CHANGE_DATA_INVOICE_PRODUCTS,
        name:name,
        value:value,
        index:index,
        products:products
    }
}
export function addRow(){
    return {
        type:ADD_ROW_PRODUCT
    }
}
export function searchInvoicesList(name){
    return {
        type :SEARCH_INVOICES,
        name:name
    }
}
export function detailInvoicesList(invoice){
    return {
        type :DETAIL_INVOICES,
        invoice:invoice
    }
}
export function addInvoiceData(data){
    return{
        type:NEW_INVOICE,
        data:data
    }
}
export function paginationInvoicesList(nbrPage){
    return {
        type :PAGINATION_INVOICE,
        nbrPage:nbrPage
    }
}
export function putInvoice(product){
    return{
        type:UPDATE_INVOICE,
        product:product
    }
}
export function putInvoiceFailed(){
    return{
        type:UPDATE_INVOICE_FAILED
    }
}
export function fetchStarted(){
    return{
        type:FETCH_START
    }
}
export function changeInvoice(name,value){
    return{
        type:CHANGE_VALUE_INVOICE,
        name:name,
        value:value
    }
}
export function dataInvoice(name,value){
    return{
        type:CHANGE_DATA_INVOICE,
        name:name,
        value:value
    }
}
export function deleteOneInvoice(id){
    return{
        type:DELETE_INVOICE,
        id:id
    }
}
export function addNewInvoice(invoice){
    return{
        type:ADD_INVOICE,
        product:invoice

    }
}