
import { history } from '../_helpers';
import { REGISTER_SUCCESS,REGISTER_FAILED,USER_INFORMATION} from "./types.js";
import { Service } from '../_services';
export const userActions = {
    register,
    getInformations
};

function register(user){
    return dispatch => {
        let apiEndpoint = 'users';
        let payload =user
        console.log(payload);
       Service.post(apiEndpoint, payload)   
        .then((response)=>{
            if (response.status===200) {
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('auth', response.data.auth);
                dispatch(setUserDetails(response.data));
                history.push('/home');
        }
          if(response.status===201){
              dispatch(userExist(response.data.message));
          }   
    })
        .catch((err)=>{
            console.log(err);
        })
    };
}
function getInformations(id){
    return dispatch=>{
        let apiEndpoint='users/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            if(response.data.data){
                dispatch(getUser(response.data.data));
            }
        })
        .catch((err)=>{
            console.log(err);
        })
    }
}
export function setUserDetails(user){
    return{
        type: REGISTER_SUCCESS,
        auth: user.auth,
        token: user.token
    }
}
export function userExist(message){
    return {
        type:REGISTER_FAILED,
        message:message
    }
}

export function getUser(user){
return{
    type:USER_INFORMATION,
    user:user
}
}