import { Service } from '../_services/';
import {FETECHED_ALL_PRODUCT,ADD_PRODUCT,PRODUCT_START,ORDER_PRODUCTS,CHANGE_DATA_PRODUCT,SEARCH_PRODUCTS,DELETE_PRODUCT,DETAIL_PRODUCTS,PAGINATION_PRODUCT,UPDATE_PRODUCT,UPDATE_PRODUCT_FAILED,CHANGE_VALUE_PRODUCT} from './types';
import { getIdUser } from '../_services/UserServices';
export const productActions = {
    getProducts,
    orderProducts,
    searchProduct,
    detailProduct,
    productPagination,
    updateProduct,
    changeValueProduct,
    deleteProduct,
    changeDataProduct,
    addProduct
};

function getProducts(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'products/'+id;
        dispatch(fetchDataStart());
        console.log(apiEndpoint);
        Service.get(apiEndpoint)
        .then((response)=>{
            if(response.data.data){
            dispatch(changeProductsList(response.data.data));
            console.log(response);
            }
        })
        .catch((err)=>{
            console.log("warning");
            console.log(err);
        });
}
}
function addProduct(product){
    return dispatch => {
        let apiEndpoint = 'products';
        let payload=product;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            dispatch(addNewProduct(product));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function orderProducts(val,order){

    return dispatch=>{
        dispatch(orderProductsList(val,order));
    }
}
function changeValueProduct(name,value){
    return dispatch=>{
        dispatch(changeProduct(name,value));
    }
}
function changeDataProduct(name,value){
    return dispatch=>{
        dispatch(dataProduct(name,value));
    }
}
function searchProduct(productName){
    return dispatch=>{
        dispatch(searchProductsList(productName));
    }
}
function productPagination(nbrPage){
    console.log(nbrPage);
    return dispatch=>{
        dispatch(paginationProductsList(nbrPage));
    }
}
function detailProduct(product){
    return dispatch=>{
        dispatch(detailProductsList(product));
    }
}
function updateProduct(id,product){
    return dispatch=>{
        let apiEndpoint = 'products/'+id;
        let payload =product;
        console.log(payload);
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            if(response.status===200){
            dispatch(putProduct(product));
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(putProductFailed());
        })
    }
}
function deleteProduct(id){
    return dispatch =>{
        let apiEndpoint = 'products/'+id;
        Service.deleteDetail(apiEndpoint)
        .then((res)=>{
            if(res.status===200){
                dispatch(deleteOneProduct(id));
            }
        })
        .catch((err)=>console.log(err))
    }
}
export function changeProductsList(products){
    return{
        type: FETECHED_ALL_PRODUCT,
        products: products
    }
}
export function orderProductsList(val,order){
    return {
        type :ORDER_PRODUCTS,
        valeur:val,
        order:order
    }
}
export function searchProductsList(name){
    return {
        type :SEARCH_PRODUCTS,
        name:name
    }
}
export function fetchDataStart(){
 return{
     type:PRODUCT_START
 }
}
export function detailProductsList(product){
    return {
        type :DETAIL_PRODUCTS,
        product:product
    }
}
export function paginationProductsList(nbrPage){
    return {
        type :PAGINATION_PRODUCT,
        nbrPage:nbrPage
    }
}
export function putProduct(product){
    return{
        type:UPDATE_PRODUCT,
        product:product
    }
}
export function putProductFailed(){
    return{
        type:UPDATE_PRODUCT_FAILED
    }
}
export function changeProduct(name,value){
    return{
        type:CHANGE_VALUE_PRODUCT,
        name:name,
        value:value
    }
}
export function dataProduct(name,value){
    return{
        type:CHANGE_DATA_PRODUCT,
        name:name,
        value:value
    }
}
export function deleteOneProduct(id){
    return{
        type:DELETE_PRODUCT,
        id:id
    }
}
export function addNewProduct(product){
    return{
        type:ADD_PRODUCT,
        product:product

    }
}