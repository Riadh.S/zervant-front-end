import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { authActions } from '../_actions';
import { connect } from 'react-redux';
class Navbar extends Component{
  constructor(props){
  super(props);
    this.state = { user:'' }
  }

logout = (e)=>{
  const { dispatch } = this.props;
  console.log(this.props);
  console.log(localStorage.getItem('auth'));
  dispatch(authActions.logout());
}
  render(){
    return ( 
      <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
          <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link text-white" to="/home">Home </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-white" to="/products">Products</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-white" to="/customers" tabindex="-1">Customers</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-white" to="/invoice" tabindex="-1">Invoice</Link>
          </li>
            <li className="nav-item">
            <Link className="nav-link text-white" to="/estimation">Estimation</Link>
          </li>
         </ul>
       <form class="form-inline my-2 my-lg-0">
       <ul className="navbar-nav mr-auto">
       <li className="nav-item">
            <Link className="nav-link text-white" to="/technicalSupport">Support</Link>
          </li>
          <li className="navbar-nav ml-auto navbar-right">
            <Link className="nav-link mr-auto text-white" to="/account">My account</Link>
          </li>
         <li className="navbar-nav ml-auto navbar-right">
         <button className="nav-link link right text-white" onClick={(e)=>this.logout()} >Logout</button>
       </li>
       </ul>
    </form>
      </div>
    </nav>
     );
}
}
const mapStateToProps = (state) =>{
  const { loggingIn } = state.authentication;
  return {
      loggingIn
  };
}
export default connect(mapStateToProps)(Navbar);