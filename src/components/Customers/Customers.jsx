import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { deleteCustomer } from "../../_services/CustomerServices";
import AddCustomer from "./AddCustomer";
import SearchCustomer from "./SearchCustomer";
import DetailCustomer from "./DetailCustomer";
import _ from "lodash";
import Pagination from "../../commons/Pagination";
import { connect } from "react-redux";
import { customerActions } from "../../_actions";

class Customers extends Component {
  state = {
    show: false
  };
  componentWillMount() {
    /* const { dispatch } = this.props;
    this.props.getCustomers();*/
    const { dispatch } = this.props;
    dispatch(customerActions.getCustomers());
  }
  handleShow = () => {
    this.setState({ show: true });
  };
  handleClick = () => {
    this.setState({ show: false });
  };
  handleChange = nbPage => {
    this.setState({ currentPage: nbPage });
  };
  handleClose = () => {
    this.setState({ show: false });
  };
  handleSearch = e => {
    console.log("onSearch", e.target.value);
    this.setState({ searchData: e.target.value });
  };
  handleDetailCustomer = customer => {
    const detailCustomer = customer;
    this.setState({ detailCustomer });
  };
  handleDelete = id => {
    const customers = this.props.customersList.filter(
      customer => customer._id !== id
    );
    this.setState({ customers, detailCustomer: "" });
    const result = deleteCustomer(id);
    console.log(result);
  };
  handleUpdate = (previousCustomer, customer) => {
    const customers = this.props.customersList;
    const index = customers.indexOf(previousCustomer);
    customers[index] = { ...customer };
    this.setState({ customers });
  };
  handleAdd = customer => {
    console.log(customer);
    this.props.customersList.push(customer);
    const customersList = this.props.customersList;
    this.setState({ customersList });
  };
  handleOrder = val => {
    const customers = _.orderBy(this.props.customersList, val, "asc");
    this.setState({ customers });
  };
  render() {
    const {
      customersList,
      searchData,
      detailCustomer,
      listPage,
      currentPage
    } = this.props;
    const customersData =
      searchData !== ""
        ? customersList.filter(customer =>
            customer.firstName.startsWith(searchData)
          )
        : customersList;
    //const length = customersData.length;
    const end = currentPage * listPage;
    const start = end - listPage;
    const MyCustomers = customersData.slice(start, end);
    return (
      <div>
        <div className="row m-t-8">
          <div className="col-sm-6">
            <section className="content">
              <div className="box">
                <div className="box-header">
                  <div className="row">
                    <div className="col-sm-4">
                      <h3 className="box-title m-t-6">List of customers</h3>
                    </div>
                    <div className="col-sm-4">
                      <SearchCustomer onSearch={this.handleSearch} />
                    </div>
                    <div className="col-sm-2 m-r-12">
                      <Button
                        className="btn btn-primary m-t-6"
                        onClick={this.handleShow}
                      >
                        Add new customer
                      </Button>
                    </div>
                  </div>
                </div>
                <div className="box-body m-t-6">
                  <table className="table">
                    <thead>
                      <tr>
                        <th
                          scope="col"
                          className="text-info"
                          onClick={() => this.handleOrder("_id")}
                        >
                          NO
                        </th>
                        <th
                          scope="col"
                          className="text-info"
                          onClick={() => this.handleOrder("firstName")}
                        >
                          Name
                        </th>
                        <th
                          scope="col"
                          className="text-info"
                          onClick={() => this.handleOrder("email")}
                        >
                          Email
                        </th>
                      </tr>
                    </thead>
                    <thead>
                      {MyCustomers.length === 0 && (
                        <tr className="alert alert-danger" role="alert">
                          <th> You don't have any customer</th>
                        </tr>
                      )}
                      {MyCustomers &&
                        customersList.map(customer => (
                          <tr
                            key={customer._id}
                            onClick={() => this.handleDetailCustomer(customer)}
                          >
                            <th className="text-secondary">
                              {customer.firstName}
                            </th>
                            <th className="text-secondary">{customer._id}</th>
                            <th className="text-secondary">
                              {customer.lastName}
                            </th>
                            <th className="text-secondary">{customer.email}</th>
                          </tr>
                        ))}
                    </thead>
                  </table>
                </div>
              </div>
            </section>
            <Pagination
              length={customersData.length}
              static={currentPage}
              onChange={this.handleChange}
              nbrPage={listPage}
            />
          </div>
          <div className="col-md-6 col-md-offset-2 bg-light">
            <div className="panel panel-default">
              <div className="panel-body">
                {detailCustomer && (
                  <DetailCustomer
                    dataBase={detailCustomer}
                    onHandleDelete={this.handleDelete}
                    onHandleUpdate={this.handleUpdate}
                  />
                )}
                {!detailCustomer && (
                  <div className="card text-center">
                    <div className="card-header">customer</div>
                    <div className="card-body">
                      <h5 className="card-title">Select customer</h5>
                      <p className="card-text">
                        select one of list customers for more details .
                      </p>
                      <br />
                      <br />
                      <a href="#!" className="btn btn-primary">
                        import customers
                      </a>
                      <br />
                      <br />
                    </div>
                    <div className="card-footer text-muted" />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <AddCustomer
          status={this.state.show}
          onHandleClose={this.handleClose}
          onHandleAdd={this.handleAdd}
        />
      </div>
    );
  }
}
/*function mapStateToProps({ customers }) {
  const { customersList } = customers;
  return { customersList };
}
export default connect(
  mapStateToProps,
  { getCustomers }
)(Customers);*/
const mapStateToProps = ({ customers }) => {
  const {
    customersList,
    searchData,
    detailCustomer,
    listPage,
    currentPage,
    data,
    customer
  } = customers;
  return {
    customersList,
    searchData,
    detailCustomer,
    listPage,
    currentPage,
    data,
    customer
  };
};
export default connect(
  mapStateToProps,
  null,
  null,
  {
    pure: false
  }
)(Customers);
