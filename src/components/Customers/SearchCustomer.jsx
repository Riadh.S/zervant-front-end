import React, { Component } from "react";
import { customerActions } from "../../_actions";
import { connect } from "react-redux";
class SearchCustomer extends Component {
  constructor(props) {
    super(props);
  }
  handleSearch = e => {
    const searched = e.target.value;
    const { dispatch } = this.props;
    dispatch(customerActions.searchCustomer(searched));
  };

  render() {
    const { searchData } = this.props.customer;
    return (
      <form>
        <div className="form-row">
          <div className="form-group col-md-12 m-t-6">
            <input
              type="text"
              value={searchData}
              className="form-control"
              onChange={e => this.handleSearch(e)}
              placeholder="Search"
            />
          </div>
        </div>
      </form>
    );
  }
}
const mapStateToProps = ({ customers }) => {
  const {
    customersList,
    searchData,
    detailCustomer,
    listPage,
    currentPage,
    data,
    customer
  } = customers;
  return {
    customersList,
    searchData,
    detailCustomer,
    listPage,
    currentPage,
    data,
    customer
  };
};
export default connect(
  mapStateToProps,
  null,
  null,
  {
    pure: false
  }
)(SearchCustomer);
