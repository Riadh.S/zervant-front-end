import React from "react";
import { Modal, Button } from "react-bootstrap";
import Form from "../../commons/Form";
import Joi from "joi-browser";
import { getIdUser } from "../../_services/UserServices";
import { connect } from "react-redux";
import { customerActions } from "../../_actions";
import SelectInput from "../../commons/SelectInput";
import CheckBox from "./../../commons/CheckBox";
import "./css/util.css";
import "./css/main.css";

class AddCustomer extends Form {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        DeliveryMethod: "",
        TypeOfCustomer: "",
        CompanyName: "",
        CompanyRegistrationNumber: "",
        VatNumber: "",
        title: "",
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        mobilePhone: "",
        address: "",
        postCode: "",
        city: "",
        country: "",
        invoicingLanguage: "",
        paymentTerm: "",
        eInvoiceAddress: "",
        eInvoiceOperator: "",
        eInvoiceNetworkType: ""
      },

      errors: ""
    };
  }
  schema = {
    DeliveryMethod: Joi.string().required(),
    TypeOfCustomer: Joi.string().required(),
    CompanyName: Joi.string().required(),
    CompanyRegistrationNumber: Joi.number().required(),
    VatNumber: Joi.number().required(),
    title: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string()
      .required()
      .email(),
    phone: Joi.number().required(),
    mobilePhone: Joi.number().required(),
    address: Joi.string().required(),
    postCode: Joi.number().required(),
    city: Joi.string().required(),
    country: Joi.string().required(),
    invoicingLanguage: Joi.string().required(),
    paymentTerm: Joi.string().required(),
    eInvoiceAddress: Joi.string().required(),
    eInvoiceOperator: Joi.string().required(),
    eInvoiceNetworkType: Joi.string().required()
  };
  validate = () => {
    // const { data } = this.props.customer;
    // const customer = {
    //   DeliveryMethod: data.DeliveryMethod,
    //   TypeOfCustomer: data.TypeOfCustomer,
    //   CompanyRegistrationNumber: data.CompanyRegistrationNumber,
    //   VatNumber: data.VatNumber,
    //   title: data.title,
    //   firstName: data.firstName,
    //   lastName: data.lasstName,
    //   email: data.email,
    //   phone: data.phone,
    //   mobilePhone: data.mobilePhone,
    //   address: data.address,
    //   postCode: data.postCode,
    //   city: data.city,
    //   country: data.country,
    //   invoicingLanguage: data.invoicingLanguage,
    //   paymentTerm: data.paymentTerm,
    //   eInvoiceAddress: data.eInvoiceAddress,
    //   eInvoiceOperator: data.eInvoiceOperator,
    //   eInvoiceNetworkType: data.eInvoiceNetworkType
    // };
    console.log("this.props.data", this.props.data);
    const resultat = Joi.validate(this.props.data, this.schema, {
      abortEarly: false
    });
    if (!resultat.error) return null;
    const errors = {};
    for (let item of resultat.error.details)
      errors[item.path[0]] = item.message;
    return errors;
  };
  // handleSubmit = e => {
  //   e.preventDefault();
  //   const { data } = this.props.customer;
  //   console.log("this.props.customer", data);
  //   const errors = this.validate();
  //   this.setState({ errors: errors || {} });
  //   if (errors) return;
  //   console.log("type of errors", errors);
  //   this.doSubmit();
  // };

  handleChange = e => {
    const { dispatch } = this.props;
    dispatch(customerActions.changeDataCustomer(e.target.name, e.target.value));
  };
  handleStar = e => {
    const { dispatch } = this.props;
    dispatch(
      customerActions.addStarDeliveryMethod(e.target.value, e.target.payload)
    );
  };

  handleCheck = (value, name) => {
    console.log(name + value);
    const { dispatch } = this.props;
    dispatch(customerActions.changeCheckBoxCustomer(name, value));
  };

  doSubmit() {
    const { data } = this.props.customer;
    console.log(data);
    if (!this.state.errors) {
      const customer = {
        id_user: getIdUser(),

        /* DeliveryMethod: {
          valuePD: data.DeliveryMethod.valuePD,
          displayPD: data.DeliveryMethod.displayPD
        },*/
        TypeOfCustomer: data.TypeOfCustomer,
        CompanyName: data.CompanyName,
        CompanyRegistrationNumber: data.CompanyRegistrationNumber,
        VatNumber: data.VatNumber,
        title: data.title,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        phone: data.phone,
        mobilePhone: data.mobilePhone,
        address: data.address,
        postCode: data.postCode,
        city: data.city,
        country: data.country,
        invoicingLanguage: data.invoicingLanguage,
        paymentTerm: data.paymentTerm,
        eInvoiceAddress: data.eInvoiceAddress,
        eInvoiceOperator: data.eInvoiceOperator,
        eInvoiceNetworkType: data.eInvoiceNetworkType
      };
      console.log(customer);
      console.log("joi marche");
      const { dispatch } = this.props;
      dispatch(customerActions.addCustomer(customer));
    } else {
      const errors = "";
      this.setState({ errors });
      console.log("probleme joi");
    }
  }
  render() {
    const { status, onHandleClose, data, customer } = this.props;
    const { errors } = this.state;
    console.log("data", data);
    return (
      <Modal show={status} onHide={onHandleClose}>
        <Modal.Header className="bg-info" closeButton>
          <Modal.Title>Add new customer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-8">
              <form onSubmit={this.handleSubmit}>
                {customer && (
                  <div
                    className="wrap-input100 alert alert-danger m-b-16"
                    role="alert"
                  >
                    {customer}
                  </div>
                )}
                <div>
                  <p>
                    Select your delivery method to see the required information
                    fields (*)
                  </p>

                  <CheckBox
                    // value={data.ByEmail}
                    name="ByEmail"
                    label="By email"
                    onHandleChange={() =>
                      this.handleCheck(data.ByEmail, "ByEmail")
                    }
                    onHandleStar={() =>
                      this.handleStar(data.CompanyName, "Company Name" + "*")
                    }
                  />
                  <CheckBox
                    // value={data.ByPost}
                    name="ByPost"
                    onHandleChange={() =>
                      this.handleCheck(data.ByPost, "ByPost")
                    }
                    label="By post"
                  />
                  <CheckBox
                    // value={data.AsEinvoice}
                    name="AsEinvoice"
                    onHandleChange={() =>
                      this.handleCheck(data.AsEinvoice, "AsEinvoice")
                    }
                    label="As e-invoice"
                  />
                  <CheckBox
                    //value={data.None}
                    name="None"
                    onHandleChange={() => this.handleCheck(data.None, "None")}
                    label="None"
                  />
                </div>
                <div>
                  <SelectInput
                    value={["New company customer", "New private customer"]}
                    name="TypeOfCustomer"
                    onHandleSelect={this.handleChange}
                    label="Type Of Customer"
                  />

                  <div className="text-center text-uppercase font-weight-bold">
                    <em>Contact Details</em>
                  </div>
                </div>
                {this.inputForm(
                  data.CompanyName,
                  "text",
                  "CompanyName",
                  "Company Name",
                  errors.CompanyName
                )}
                {this.inputForm(
                  data.CompanyRegistrationNumber,
                  "text",
                  "CompanyRegistrationNumber",
                  "Company Registration Number",
                  errors.CompanyRegistrationNumber
                )}
                {this.inputForm(
                  data.VatNumber,
                  "text",
                  "VatNumber",
                  "VAT Number",
                  errors.VatNumber
                )}
                {this.inputForm(
                  data.vat,
                  "text",
                  "title",
                  "Title",
                  errors.title
                )}
                {this.inputForm(
                  data.firstName,
                  "text",
                  "firstName",
                  "First Name",
                  errors.firstName
                )}
                {this.inputForm(
                  data.lastName,
                  "text",
                  "lastName",
                  "Last Name",
                  errors.lastName
                )}
                {this.inputForm(
                  data.email,
                  "text",
                  "email",
                  "Email",
                  errors.email
                )}
                {this.inputForm(
                  data.phone,
                  "text",
                  "phone",
                  "Phone",
                  errors.phone
                )}
                {this.inputForm(
                  data.mobilePhone,
                  "text",
                  "mobilePhone",
                  "Mobile Phone",
                  errors.mobilePhone
                )}
                <div className="text-center text-uppercase font-weight-bold">
                  Invoicing address
                </div>
                {this.inputForm(
                  data.address,
                  "text",
                  "address",
                  "Address",
                  errors.address
                )}
                {this.inputForm(
                  data.postCode,
                  "text",
                  "postCode",
                  "Post Code",
                  errors.postCode
                )}
                {this.inputForm(data.city, "text", "city", "City", errors.city)}
                <SelectInput
                  value={["Tunisia", "France", "Deutch"]}
                  name="country"
                  onHandleSelect={this.handleChange}
                  label="Country"
                />
                <div className="text-center text-uppercase font-weight-bold">
                  Invoicing Details
                </div>
                <SelectInput
                  value={["french", "English"]}
                  name="invoicingLanguage"
                  onHandleSelect={this.handleChange}
                  label="Invoicing Language"
                />
                <SelectInput
                  value={["30", "60", "90"]}
                  name="paymentTerm"
                  onHandleSelect={this.handleChange}
                  label="Payment Terms"
                />
                <div className="text-center text-uppercase font-weight-bold">
                  E-Invoice Address
                </div>
                {this.inputForm(
                  data.eInvoiceAddress,
                  "text",
                  "eInvoiceAddress",
                  "E-Invoice Address",
                  errors.eInvoiceAddress
                )}
                <SelectInput
                  value={["4 solutions", "3E consultancy solutions"]}
                  name="eInvoiceOperator"
                  onHandleSelect={this.handleChange}
                  label="E-Invoice Operator"
                />
                <SelectInput
                  value={["Public PEPPOL network", "Private operator network"]}
                  name="eInvoiceNetworkType"
                  onHandleSelect={this.handleChange}
                  label="E-Invoice Network Type"
                />
                {this.buttonSubmit("Submit")}
              </form>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="bg-info ">
          <Button variant="secondary" onClick={onHandleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = ({ customers }) => {
  const { data, customer } = customers;
  return {
    data,
    customer
  };
};
export default connect(
  mapStateToProps,
  null,
  null,
  {
    pure: false
  }
)(AddCustomer);
