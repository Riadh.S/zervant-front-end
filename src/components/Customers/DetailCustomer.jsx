import React from "react";
import { Button, Modal } from "react-bootstrap";
import Form from "../../commons/Form";
import { putCustomer } from "../../_services/CustomerServices";
import Joi from "joi-browser";
import { getIdUser } from "../../_services/UserServices";
class DetailCustomer extends Form {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      data: {
        CompanyName: props.dataBase.CompanyName,
        CompanyRegistrationNumber: props.dataBase.CompanyRegistrationNumber,
        VatNumber: props.dataBase.VatNumber,
        title: props.dataBase.title,
        firstName: props.dataBase.firstName,
        lastName: props.dataBase.lastName,
        email: props.dataBase.email,
        phone: props.dataBase.phone,
        mobilePhone: props.dataBase.mobilePhone,
        address: props.dataBase.adress,
        postCode: props.dataBase.postCode,
        city: props.dataBase.city,
        country: props.dataBase.country,
        invoicingLanguage: props.dataBase.invoicingLanguage,
        paymentTerm: props.dataBase.paymentTerm,
        eInvoiceAddress: props.dataBase.eInvoiceAdress,
        eInvoiceOperator: props.dataBase.eInvoiceOperator,
        eInvoiceNetworkType: props.dataBase.eInvoiceNetworkType
      },
      errors: "",
      customers: "",
      deleteCustomers: false
    };
  }
  schema = {
    CompanyName: Joi.string()
      .required()
      .min(2)
      .max(50)
      .label("Company Name"),
    CompanyRegistrationNumber: Joi.string()
      .required()
      .min(2)
      .max(50)
      .label("Company Registreation Number"),
    VatNumber: Joi.number()
      .min(0)
      .max(100)
      .required()
      .label("VAT Number"),
    title: Joi.string()
      .min(2)
      .max(50)
      .required()
      .label("Title"),
    firstName: Joi.string()
      .min(2)
      .max(50)
      .required()
      .label("First Name"),
    lastName: Joi.string()
      .min(2)
      .max(50)
      .required()
      .label("Last Name"),
    email: Joi.string()
      .required()
      .email()
      .label("Email"),
    phone: Joi.number()
      .required()
      .min(8)
      .label("Phone"),
    mobilePhone: Joi.number()
      .required()
      .min(8)
      .label("Mobile Phone"),
    address: Joi.string()
      .min(10)
      .max(150)
      .required()
      .label("Adress"),
    postCode: Joi.number()
      .min(3)
      .max(5)
      .required()
      .min(4)
      .label("Post Code"),
    city: Joi.string()
      .min(2)
      .max(50)
      .required()
      .label("City"),
    country: [String],
    invoicingLanguage: [String],
    paymentTerm: [String],
    eInvoiceAddress: [String], // à vérifier pour ajouter texte
    eInvoiceOperator: [String],
    eInvoiceNetworkType: [String]
  };
  handleCloseDelete = () => {
    this.setState({ deleteCustomers: false });
  };
  async doSubmit() {
    if (!this.state.errors) {
      const customers = {
        id_user: getIdUser(),
        CompanyName: this.state.data.CompanyName,
        CompanyRegistrationNumber: this.state.data.CompanyRegistrationNumber,
        VatNumber: this.state.data.VatNumber,
        title: this.state.data.title,
        firstName: this.state.data.firstName,
        lastName: this.state.data.lastName,
        email: this.state.data.email,
        phone: this.state.data.phone,
        mobilePhone: this.state.data.mobilePhone,
        address: this.state.data.adress,
        postCode: this.state.data.postCode,
        city: this.state.data.city,
        country: this.state.data.country,
        invoicingLanguage: this.state.data.invoicingLanguage,
        paymentTerm: this.state.data.paymentTerm,
        eInvoiceAddress: this.state.data.eInvoiceAdress,
        eInvoiceOperator: this.state.data.eInvoiceOperator,
        eInvoiceNetworkType: this.state.data.eInvoiceNetworkType
      };
      const previousCustomers = this.props.dataBase;
      const id = this.props.dataBase._id;
      const response = await putCustomer(id, customers);
      console.log(response);
      if (response === true) {
        console.log("updated");
        this.props.onHandleUpdate(previousCustomers, customers);
      } else {
        const customers = "something wrong";
        this.setState({ customers });
      }
    } else {
      const customers = "";
      const errors = "";
      this.setState({ errors, customers });
    }
  }
  delete = () => {
    this.setState({ deleteCustomers: true });
  };
  handleShow = () => {
    const data = {
      CompanyName: this.props.data.CompanyName,
      CompanyRegistrationNumber: this.props.data.CompanyRegistrationNumber,
      VatNumber: this.props.data.VatNumber,
      title: this.props.data.title,
      firstName: this.props.data.firstName,
      lastName: this.props.data.lastName,
      email: this.props.data.email,
      phone: this.props.data.phone,
      mobilePhone: this.props.data.mobilePhone,
      address: this.props.data.adress,
      postCode: this.props.data.postCode,
      city: this.props.data.city,
      country: this.props.data.country,
      invoicingLanguage: this.props.data.invoicingLanguage,
      paymentTerm: this.props.data.paymentTerm,
      eInvoiceAddress: this.props.data.eInvoiceAdress,
      eInvoiceOperator: this.props.data.eInvoiceOperator,
      eInvoiceNetworkType: this.props.data.eInvoiceNetworkType
    };
    this.setState({ show: true, data });
  };
  handleClose = () => {
    this.setState({ show: false });
  };
  render() {
    const { dataBase, onHandleDelete } = this.props;
    const { data, errors, customers } = this.state;
    return (
      <div className="card text-center m-l-8">
        <div className="card-header">customers</div>
        <div className="card-body">
          <h5 className="card-title">{dataBase.CompanyName}</h5>
          <p className="card-text"> {dataBase.CompanyRegistrationNumber}</p>
          <p className="card-text"> {dataBase.VatNumber}</p>
          <p className="card-text"> {dataBase.title}</p>
          <p className="card-text"> {dataBase._id}</p>
          <p className="card-text"> {dataBase.firstName}</p>
          <p className="card-text"> {dataBase.lastName}</p>
          <p className="card-text"> {dataBase.email}</p>
          <p className="card-text"> {dataBase.phone}</p>
          <p className="card-text"> {dataBase.mobilePhone}</p>
          <p className="card-text"> {dataBase.address}</p>
          <p className="card-text"> {dataBase.postCode}</p>
          <p className="card-text"> {dataBase.city}</p>
          <p className="card-text"> {dataBase.country}</p>
          <p className="card-text"> {dataBase.invoicingLanguage}</p>
          <p className="card-text"> {dataBase.paymentTerm}</p>
          <p className="card-text"> {dataBase.eInvoiceAddress}</p>
          <p className="card-text"> {dataBase.eInvoiceOperator}</p>
          <p className="card-text"> {dataBase.eInvoiceNetworkType}</p>
          <br>
            <p>
              <u>
                <em>INVOICING DETAILS </em>
              </u>
            </p>
          </br>
          <p className="card-text">Email {dataBase.email}</p>
          <br>
            {" "}
            <p className="card-text">
              Invoicing language{dataBase.invoicingLanguage}
            </p>
          </br>
          <br />
          <br />
          <br />
          <br />
          <br />
        </div>
        <div className="card-footer text-muted">
          <Button className="btn btn-info m-l-4" onClick={this.handleShow}>
            Edit
          </Button>
          <button className="btn btn-danger m-l-4" onClick={this.delete}>
            delete
          </button>
        </div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header className="bg-info" closeButton>
            <Modal.Title>Update customers</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-lg-8">
                <form onSubmit={this.handleSubmit}>
                  {customers && (
                    <div
                      className="wrap-input100 alert alert-danger m-b-16"
                      role="alert"
                    >
                      {customers}
                    </div>
                  )}
                  {this.inputForm(data._id, "text", "_id", "_id", errors._id)}
                  {this.inputForm(
                    data.firstName,
                    "text",
                    "firstName",
                    "firstName",
                    errors.firstName
                  )}
                  {this.inputForm(
                    data.lastName,
                    "text",
                    "lastName",
                    "lastName",
                    errors.lastName
                  )}
                  {this.inputForm(
                    data.email,
                    "text",
                    "email",
                    "email",
                    errors.email
                  )}

                  {this.buttonSubmit("Update")}
                </form>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="bg-info">
            <Button variant="secondary" onClick={this.handleCloseDelete}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={this.state.deleteCustomers}
          onHide={this.handleCloseDelete}
        >
          <Modal.Header className="bg-info" closeButton>
            <Modal.Title>Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are You sure!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseDelete}>
              Close
            </Button>
            <Button
              variant="primary"
              onClick={() => onHandleDelete(dataBase._id)}
            >
              Confirm
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default DetailCustomer;
