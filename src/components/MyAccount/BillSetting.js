import React from 'react';
import FormNotRequired from '../../commons/FormNotRequired';
import  MultiSelectReact  from 'multi-select-react';
import {connect} from 'react-redux';
import InputNotRequired from '../../commons/InputNotRequired';
import SelectInput from '../../commons/SelectInput';
import Textaria from '../../commons/Textaria';
import CheckBox from '../../commons/CheckBox';
import {billSettingActions} from '../../_actions';
import { getIdUser } from '../../_services/UserServices';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class BillSetting extends FormNotRequired {
    constructor(props) {
        super(props);
    }
    handleClicked =(optionsList)=>{
      const discountSetting=optionsList;
      this.setState({discountSetting });
    }
    handleBadgeClicked=(optionsList)=>{
      const discountSetting=optionsList;
      this.setState({discountSetting });
    }
    optionClicked=(optionsList) =>{
      const displayColunms=optionsList;
      this.setState({displayColunms });
}
selectedBadgeClicked=(optionsList)=> {
  const displayColunms=optionsList;
      this.setState({ displayColunms });
}
componentWillMount(){
  const {dispatch}=this.props;
  dispatch(billSettingActions.getInformationBillSetting());
}
handleSubmit=(e)=>{
  e.preventDefault();
  console.log(this.props.billSetting);
 this.doSubmit();        
}
doSubmit=()=>{
  const {found,data}=this.props.billSetting;
  if(found===true){
    const bill={
      id_user:getIdUser(),
     currency:data.currency,
        language:data.language,
        billHeader:data.billHeader,
        billType:data.billType,
        paymentCondition:{valuePD:data.paymentCondition.valuePD,displayPD:data.paymentCondition.displayPD},
        delayInterest:{valueDI:data.delayInterest.valueDI,displayDI:data.delayInterest.displayDI},
        paymentRef:data.paymentRef,
        attentionOf:data.attentionOf,
        displayColunms:data.displayColunms ,
        discountSetting:data.discountSetting,
        message:data.message,
        notesFooter:data.notesFooter
    }
    const {dispatch}=this.props;
    const id=data._id;
    dispatch(billSettingActions.updateDataBill(bill,id));
  }
  else{
   const bill={
     id_user:getIdUser(),
     currency:data.currency,
        language:data.language,
        billHeader:data.billHeader,
        billType:data.billType,
        paymentCondition:{valuePD:data.paymentCondition.valuePD,displayPD:data.paymentCondition.displayPD},
        delayInterest:{valueDI:data.delayInterest.valueDI,displayDI:data.delayInterest.displayDI},
        paymentRef:data.paymentRef,
        attentionOf:data.attentionOf,
        displayColunms:data.displayColunms ,
        discountSetting:data.discountSetting,
        message:data.message,
        notesFooter:data.notesFooter    
      }
    console.log(bill);
    const {dispatch}=this.props;
    dispatch(billSettingActions.addBill(bill));
  }
}
handleCheck=(valeur,name)=>{
  console.log(name+valeur)
  const {dispatch}=this.props;
  dispatch(billSettingActions.changeCheckBoxBillSetting(name,valeur));
}
handleChange=(e)=>{
  const {dispatch}=this.props;
  dispatch(billSettingActions.changeDataBillSetting(e.target.name,e.target.value));
}
    render() { 
        const {data}=this.props.billSetting;
        console.log(data);
        const selectedOptionsStyles = {
          color: "#fff",
          backgroundColor: "#3498bd"
        };
      const optionsListStyles = {
          backgroundColor: "#95a5ac",
          color: "#fff",
          size:15
          
      };
        return ( 
          <div>
             <ToastContainer /> 
            <div className="card text-center">
            <div className="card-header">
              Invoice details
            </div>
            <div className="card-body">
            <form onSubmit={this.handleSubmit}>
           <p> <SelectInput value={["USD","EUR","DTN"]} name="currency" onHandleSelect={this.handleChange}
 label="currency"/></p>
      <p><SelectInput value={["English","Dutch"]} name="language" onHandleSelect={this.handleChange}
 label="language"/></p>
    <p><InputNotRequired value={data.billHeader}  type="text" name="billHeader" onHandleChange={this.handleChange}
 label="invoice Header"/></p>         
<p><SelectInput value={["Sales","Sales excluding VAT","Sales exempt from VAT"]} name="billType" onHandleSelect={this.handleChange}
 label="invoice Type"/></p>
    <p><InputNotRequired value={data.paymentCondition.valuePD}  type="text" name="valuePD" onHandleChange={this.handleChange}
 label="payment Condition"/>
 <CheckBox value={data.paymentCondition.displayPD} name="displayPD" onHandleChange={()=>this.handleCheck(data.paymentCondition.displayPD,"displayPD")}/>
 </p> 
     <p><InputNotRequired value={data.delayInterest.valueDI}  type="text" name="valueDI" onHandleChange={this.handleChange}
 label="delay Interest"/>
  <CheckBox value={data.delayInterest.displayDI} name="displayDI" onHandleChange={()=>this.handleCheck(data.delayInterest.displayDI,"displayDI")}/>
 </p> 
 <p>  <CheckBox value={data.paymentRef} name="paymentRef" label="payment Ref" onHandleChange={()=>this.handleCheck(data.paymentRef,"paymentRef")}/></p>
 <p> <CheckBox value={data.attentionOf} name="attentionOf" label="attention Of" onHandleChange={()=>this.handleCheck(data.attentionOf,"attentionOf")}/></p>
 <p><div class="row">
 <div className="col-2"></div>
 <div className="col-2"><label>Display columns</label></div>
<div className="col-6"><MultiSelectReact optionClicked={this.optionClicked}
      selectedBadgeClicked={this.selectedBadgeClicked} options={data.displayColunms} 
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} /></div></div></p>
   <p><div class="row">
 <div className="col-2"></div>
 <div className="col-2"><label>Discount setting</label></div>
<div className="col-6"><MultiSelectReact optionClicked={this.handleClicked}
      selectedBadgeClicked={this.handleBadgeClicked} options={data.discountSetting} 
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} /></div></div></p>    
 <p>      <Textaria value={data.message} name="message" onHandleChange={this.handleChange}
label="message"/></p>
 <p>      <Textaria value={data.notesFooter} name="notesFooter" onHandleChange={this.handleChange}
label="notes Footer"/></p>

  {this.buttonSubmit("Save")}
</form>
            </div>
            <div className="card-footer text-muted">
            </div>
            </div>
          </div>  
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(BillSetting); 