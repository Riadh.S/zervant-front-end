import React from 'react';
import FormNotRequired from '../../commons/FormNotRequired';
import InputNotRequired from '../../commons/InputNotRequired';
import {connect} from 'react-redux';
import {paymentActions} from '../../_actions';
import { getIdUser } from '../../_services/UserServices';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class PaymentMethod extends FormNotRequired {
    constructor(props) {
        super(props);
    }
    componentWillMount(){
        const {dispatch}=this.props;
        dispatch(paymentActions.getInformationPayment());
      }
      handleSubmit=(e)=>{
        e.preventDefault();
       this.doSubmit();        
  }
      handleChange=(e)=>{
        const {dispatch}=this.props;
        dispatch(paymentActions.changeDataPayment(e.target.name,e.target.value));
      }
      doSubmit(){
       const {found,data}=this.props.payment;
       if(found===true){
         const payment={
            paymentMeans:data.paymentMeans,
            paypal:data.paypal ,
            bancData:{
                bancDATA:data.bancData.bancDATA,
                swiftCode:data.bancData.swiftCode,
                IBAN:data.bancData.IBAN},
            RIB:{
                bancRIB:data.RIB.bancRIB,
                codeBox:data.RIB.codeBox,
                accountNumber:data.RIB.accountNumber,
                accountHolder:data.RIB.accountHolder
            } 
         }
         const {dispatch}=this.props;
         const id=data._id;
         dispatch(paymentActions.updateDataPayment(payment,id));
       }
       else{
        const payment={
          id_user:getIdUser(),
          paymentMeans:data.paymentMeans,
            paypal:data.paypal ,
            bancData:{
                bancDATA:data.bancData.bancDATA,
                swiftCode:data.bancData.swiftCode,
                IBAN:data.bancData.IBAN},
            RIB:{
                bancRIB:data.RIB.bancRIB,
                codeBox:data.RIB.codeBox,
                accountNumber:data.RIB.accountNumber,
                accountHolder:data.RIB.accountHolder
            }
         }
         console.log(payment);
         const {dispatch}=this.props;
         dispatch(paymentActions.addPayment(payment));
       }
      }
    render() { 
        const {data}=this.props.payment;
        return ( 
          <div>
            <ToastContainer/>
            <div className="card text-center">
            <div className="card-header">
              Payment method
            </div>
            <div className="card-body">
            <form onSubmit={this.handleSubmit}>
            <p>     <InputNotRequired value={data.paymentMeans} type="text" name="paymentMeans" onHandleChange={this.handleChange}
 label="payment Method"/></p>
 <p>     <InputNotRequired value={data.paypal} type="text" name="paypal" onHandleChange={this.handleChange}
 label="paypal"/></p>
<p>     <InputNotRequired value={data.bancData.bancDATA} type="text" name="bancDATA" onHandleChange={this.handleChange}
 label="bancData : banc"/></p>
 <p>     <InputNotRequired value={data.bancData.swiftCode} type="text" name="swiftCode" onHandleChange={this.handleChange}
 label="swift Code"/></p>
 <p>     <InputNotRequired value={data.bancData.IBAN} type="text" name="IBAN" onHandleChange={this.handleChange}
 label="IBAN"/></p>
 <p>     <InputNotRequired value={data.RIB.bancRIB} type="text" name="bancRIB" onHandleChange={this.handleChange}
 label="RIB : banc"/></p>
<p>     <InputNotRequired value={data.RIB.codeBox} type="text" name="codeBox" onHandleChange={this.handleChange}
 label="codeBox"/></p>
 <p>     <InputNotRequired value={data.RIB.accountNumber} type="text" name="accountNumber" onHandleChange={this.handleChange}
 label="account Number"/></p>
<p>     <InputNotRequired value={data.RIB.accountHolder} type="text" name="accountHolder" onHandleChange={this.handleChange}
 label="account Holder"/></p> 

  {this.buttonSubmit("Save")}
</form>
            </div>
            <div className="card-footer text-muted">
            </div>
            </div>
          </div>  
         );
    }
}
const mapStateToProps = (state) =>{
    return state;
  }
  export default connect(mapStateToProps, null, null, {
    pure: false
  })(PaymentMethod);