import React from 'react';
import FormNotRequired from '../../commons/FormNotRequired';
import {connect} from 'react-redux';
import {companyActions} from '../../_actions';
import InputNotRequired from '../../commons/InputNotRequired';
import { getIdUser } from '../../_services/UserServices';
import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
class CompanyInformation extends FormNotRequired {
    constructor(props) {
        super(props);
    }
    componentWillMount(){
      const {dispatch}=this.props;
      dispatch(companyActions.getInformationCompany());
    }
    handleSubmit=(e)=>{
      e.preventDefault();
     this.doSubmit();        
}
handleImageChange(event) {
  this.setState({
    file: URL.createObjectURL(event.target.files[0])
  })
}
    handleChange=(e)=>{
      const {dispatch}=this.props;
      dispatch(companyActions.changeDataCompany(e.target.name,e.target.value));
    }
    doSubmit(){
     const {found,data}=this.props.companyInformation;
     if(found===true){
       const company={
        siretNumber:data.siretNumber,
        vatNumber:data.vatNumber,
        vatDeclaration:data.vatDeclaration,
        defaultVat:data.defaultVat,
        adress:data.adress,
        codePostal:data.codePostal,
        city:data.city,
        webSite:data.webSite,
        logo:data.logo 
       }
       const {dispatch}=this.props;
       const id=data._id;
       dispatch(companyActions.updateDataCompany(company,id));
     }
     else{
      const company={
        id_user:getIdUser(),
        siretNumber:data.siretNumber,
        vatNumber:data.vatNumber,
        vatDeclaration:data.vatDeclaration,
        defaultVat:data.defaultVat,
        adress:data.adress,
        codePostal:data.codePostal,
        city:data.city,
        webSite:data.webSite,
        logo:data.logo 
       }
       const {dispatch}=this.props;
       dispatch(companyActions.addCompany(company));
     }
    }
    render() {
      console.log(this.props.companyInformation); 
        const {data,found}=this.props.companyInformation;
        return ( 
         <div>
           <ToastContainer /> 
            <div className="card text-center">
            <div className="card-header">
              Company details
            </div>
            <div className="card-body">
            <form onSubmit={this.handleSubmit}>
 <p>     <InputNotRequired value={data.siretNumber} type="text" name="siretNumber" onHandleChange={this.handleChange}
 label="siret Number"/></p>
 <p>     <InputNotRequired value={data.vatNumber} type="text" name="vatNumber" onHandleChange={this.handleChange}
 label="vat Number"/></p>
<p>     <InputNotRequired value={data.vatDeclaration} type="text" name="vatDeclaration" onHandleChange={this.handleChange}
 label="vat Declaration"/></p>
 <p>     <InputNotRequired value={data.defaultVat} type="text" name="defaultVat" onHandleChange={this.handleChange}
 label="default Vat"/></p>
 <p>     <InputNotRequired value={data.adress} type="text" name="adress" onHandleChange={this.handleChange}
 label="adress"/></p>
 <p>     <InputNotRequired value={data.codePostal} type="text" name="codePostal" onHandleChange={this.handleChange}
 label="code Postal"/></p>
<p>     <InputNotRequired value={data.city} type="text" name="city" onHandleChange={this.handleChange}
 label="city"/></p>
 <p>     <InputNotRequired value={data.webSite} type="text" name="webSite" onHandleChange={this.handleChange}
 label="web Site"/></p>

<p> <div class="form-inline">
     <div className="col-2" ></div><label fhtmlFor="avatar" className="col-md-2 control-label" >logo</label>
       <div className="col-md-6">
      <input type="file" value={data.logo} id="picture" name="logo" accept=".jpg, .jpeg, .png"
            onChange={this.handleChange} />
             <img src={data.logo}/>
        </div>
        </div></p>
  {this.buttonSubmit("Save")}
</form>
            </div>
            <div className="card-footer text-muted">
            </div>
            </div>
          </div>  
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(CompanyInformation);