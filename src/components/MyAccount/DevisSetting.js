import React from 'react';
import FormNotRequired from '../../commons/FormNotRequired';
import Textaria from '../../commons/Textaria';
import CheckBox from '../../commons/CheckBox';
import  MultiSelectReact  from 'multi-select-react';
import { getIdUser } from '../../_services/UserServices';
import {connect} from 'react-redux';
import {devisSettingActions} from '../../_actions/devisSetting.actions';
import SelectInput from '../../commons/SelectInput';
import InputNotRequired from '../../commons/InputNotRequired';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class DevisSetting extends FormNotRequired {
    constructor(props) {
        super(props);
    }
    handleClicked =(optionsList)=>{
      const discountSetting=optionsList;
      this.setState({discountSetting });
    }
    handleBadgeClicked=(optionsList)=>{
      const discountSetting=optionsList;
      this.setState({discountSetting });
    }
    optionClicked=(optionsList) =>{
      const displayColunms=optionsList;
      this.setState({displayColunms });
}
selectedBadgeClicked=(optionsList)=> {
  const displayColunms=optionsList;
      this.setState({ displayColunms });
}
componentWillMount(){
  const {dispatch}=this.props;
  dispatch(devisSettingActions.getInformationDevisSetting());
}
handleSubmit=(e)=>{
  e.preventDefault();
  console.log(this.props.devisSetting);
 this.doSubmit();        
}
doSubmit=()=>{
  const {found,data}=this.props.devisSetting;
  if(found===true){
    const devis={
      id_user:getIdUser(),
      currency:data.currency,
      language:data.language,
      devisHeader:data.devisHeader,
      dayValidity:data.dayValidity,
      attentionOf:data.attentionOf,
      displayColunms:data.displayColunms ,
      discountSetting:data.discountSetting,
      message:data.message,
      notesFooter:data.notesFooter,
      ConditionsSale:{
        valueCS:data.ConditionsSale.valueCS,
        displayCS:data.ConditionsSale.displayCS
      } 
    }
    const {dispatch}=this.props;
    const id=data._id;
    dispatch(devisSettingActions.updateDataDevis(devis,id));
  }
  else{
   const devis={
     id_user:getIdUser(),
     currency:data.currency,
        language:data.language,
        devisHeader:data.devisHeader,
        dayValidity:data.dayValidity,
        attentionOf:data.attentionOf,
        displayColunms:data.displayColunms ,
        discountSetting:data.discountSetting,
        message:data.message,
        notesFooter:data.notesFooter,
        ConditionsSale:{
          valueCS:data.ConditionsSale.valueCS,
          displayCS:data.ConditionsSale.displayCS
        }    
      }
    console.log(devis);
    const {dispatch}=this.props;
    dispatch(devisSettingActions.addDevis(devis));
  }
}
handleCheck=(valeur,name)=>{
  console.log(name+valeur)
  const {dispatch}=this.props;
  dispatch(devisSettingActions.changeCheckBoxDevisSetting(name,valeur));
}
handleChange=(e)=>{
  const {dispatch}=this.props;
  dispatch(devisSettingActions.changeDataDevisSetting(e.target.name,e.target.value));
}
    render() { 
        const {data}=this.props.devisSetting;
        const selectedOptionsStyles = {
          color: "#fff",
          backgroundColor: "#3498bd"
        };
      const optionsListStyles = {
          backgroundColor: "#95a5ac",
          color: "#fff",
          size:15
          
      };
        return ( 
          <div>
            <ToastContainer/>
            <div className="card text-center">
            <div className="card-header">
              Estimate details
            </div>
            <div className="card-body">
            <form onSubmit={this.handleSubmit}>
            <p> <SelectInput value={["USD","EUR","DTN"]} name="currency" onHandleSelect={this.handleChange}
 label="currency"/></p>
      <p><SelectInput value={["English","Dutch"]} name="language" onHandleSelect={this.handleChange}
 label="language"/></p>
    <p><InputNotRequired value={data.devisHeader}  type="text" name="devisHeader" onHandleChange={this.handleChange}
 label="estimate Header"/></p>         
 <p>  <CheckBox value={data.dayValidity} name="dayValidity" label="day Validity" onHandleChange={()=>this.handleCheck(data.dayValidity,"dayValidity")}/></p>
 <p> <CheckBox value={data.attentionOf} name="attentionOf" label="attention Of" onHandleChange={()=>this.handleCheck(data.attentionOf,"attentionOf")}/></p>
 <p><div class="row">
 <div className="col-2"></div>
 <div className="col-2"><label>Display columns</label></div>
<div className="col-6"><MultiSelectReact optionClicked={this.optionClicked}
      selectedBadgeClicked={this.selectedBadgeClicked} options={data.displayColunms} 
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} /></div></div></p>
   <p><div class="row">
 <div className="col-2"></div>
 <div className="col-2"><label>Discount setting</label></div>
<div className="col-6"><MultiSelectReact optionClicked={this.handleClicked}
      selectedBadgeClicked={this.handleBadgeClicked} options={data.discountSetting} 
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} /></div></div></p>    
 <p>      <Textaria value={data.message} name="message" onHandleChange={this.handleChange}
label="message"/></p>
 <p>      <Textaria value={data.notesFooter} name="notesFooter" onHandleChange={this.handleChange}
label="notes Footer"/></p>
 <p>      <Textaria value={data.ConditionsSale.valueCS} name="valueCS" onHandleChange={this.handleChange}
label="Conditions Sale"/>
 <CheckBox value={data.ConditionsSale.displayCS} name="displayCS" onHandleChange={()=>this.handleCheck(data.ConditionsSale.displayCS,"displayCS")}/>
</p>

  {this.buttonSubmit("Save")}
</form>
            </div>
            <div className="card-footer text-muted">
            </div>
            </div>
          </div>  
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(DevisSetting); 