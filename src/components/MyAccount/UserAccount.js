import React from 'react';
import FormNotRequired from '../../commons/FormNotRequired';
import {connect} from 'react-redux';
import {userActions} from '../../_actions';
import { getIdUser } from '../../_services/UserServices';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class UserAccount extends FormNotRequired {
    constructor(props) {
        super(props);
    }
   async componentWillMount(){
     const id=getIdUser();
     const {dispatch}=this.props;
     dispatch(userActions.getInformations(id));
   } 
    render() { 
        const {data}=this.props.userAccount;
        return ( 
          <div>
            <ToastContainer/>
            <div className="card text-center">
            <div className="card-header">
              User details
            </div>
            <div className="card-body">
            <form onSubmit={this.handleSubmit}>
 <p>{this.inputForm(data.name,"text","name","name")}</p>
 <p>{this.inputForm(data.email,"email","email","email")}</p>
 <p> {this.inputForm(data.country,"text","country","country")}</p>
 <p> {this.inputForm(data.password,"password","password","password")}</p>
 <p> {this.inputForm(data.companyName,"text","companyName","companyName")}</p>
 <p> {this.inputForm(data.currency,"text","currency","currency")}</p>
 <p> {this.inputForm(data.language,"text","language","language")}</p>

  {this.buttonSubmit("Save")}
</form>
            </div>
            <div className="card-footer text-muted">
            </div>
            </div>
          </div>  
         );
    }   
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(UserAccount);