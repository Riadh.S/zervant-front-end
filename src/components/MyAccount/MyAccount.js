import React, { Component } from 'react';
import CompanyInformation from './CompanyInformation';
import UserAccount from './UserAccount';
import PaymentMethod from './PaymentMethod';
import BillSetting from './BillSetting';
import DevisSetting from './DevisSetting';
class MyAccount extends Component {
    state = { 
        show:'company'
     }
     handleChoice=(choice)=>{
         this.setState({show:choice});
     }
     showData=()=>{
       console.log(this.state.show);
      switch (this.state.show) {
            case 'bill':
             return <BillSetting/>;
              break;
              case 'devis':
               return <DevisSetting/>;
                break;   
                case 'payment':
                 return <PaymentMethod/>; 
                  break;
                  case 'user':
                 return <UserAccount/>;  
                  break;          
        default:
        return <CompanyInformation/> ;
     }
    }
    render() {
        return ( 
            <div className="row m-t-20 bg-light">
            <div className="col-3">
            <div className="card">
  <div className="card-header">
    My Account
  </div>
  <ul className="list-group list-group-flush">
    <li  className="list-group-item" onClick={()=>this.handleChoice("company")}>Company information</li>
    <li className="list-group-item" onClick={()=>this.handleChoice("bill")}>Bill setting</li>
    <li className="list-group-item" onClick={()=>this.handleChoice("devis")}>Devis setting</li>
    <li className="list-group-item" onClick={()=>this.handleChoice("payment")}>Payment methods</li>
    <li className="list-group-item" onClick={()=>this.handleChoice("user")}>User account</li>
  </ul>
</div>
            </div>
         
         <div className="col-9">
      {this.state.show && this.showData()}    
             </div>   
            </div>
         );
    }
}
 
export default MyAccount;