import React, { Component } from 'react';
import Joi from 'joi-browser';
import Form from '../../commons/Form';
import './css/util.css';
import './css/main.css';
import './vendor/bootstrap/css/bootstrap.min.css';
import './images/icons/favicon.ico';
import '../../assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css';
import './vendor/select2/select2.min.css';
import '../../assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css';
import './vendor/animate/animate.css';
import './vendor/css-hamburgers/hamburgers.min.css';
import img from './images/icons/icon-google.png';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { authActions } from '../../_actions';
import { history } from '../../_helpers';
class Login extends Form{
  constructor(props){
    super(props); 
  this.state={
        data:{
            email:'',
            password:''
        },
        errors:'',        
      }
    }
       schema ={    
          email: Joi.string().email().required(),    
          password: Joi.string().required()
      }
      componentDidMount() {
        if(localStorage.getItem('auth')){
            history.push('/home');
        }
    }
        doSubmit(){  
        if(!this.state.errors){
      /*  const response=await login(this.state.data.email,this.state.data.password);
        console.log(response);
          if(response===false){
           const user="user not found ,you need to register";
           this.setState({user}); */
           this.login();
     
          
    }else{
      const errors='';
      this.setState({errors});
    }
  }
  login = () =>{
    const { email, password } = this.state.data;
    const { dispatch } = this.props;
    if (email && password) {
        dispatch(authActions.login(email, password));
    }
}
    render(){
        const {data,errors}=this.state;
      const {userResponse}=this.props.authentication;

        return (
      <div className="limiter">
      <div className="container-login100">
        <div className="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
          <form className="login100-form validate-form" onSubmit={this.handleSubmit}>
            <span className="login100-form-title p-b-55">
              Login
            </span>
            {userResponse && <div className="wrap-input100 alert alert-danger m-b-16" role="alert">
{userResponse}
</div>}  
            {errors.email && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
 {errors.email}
</div>}
            <div className="wrap-input100 validate-input m-b-16" data-validate = "Valid email is required: ex@abc.xyz">
          
              <input className="input100" value={data.email} type="email" onChange={this.handleChange} name="email" placeholder="Email" />
              <span className="focus-input100"></span>
              <span className="symbol-input100">
                <span className="lnr lnr-envelope"></span>
              </span>
         
            </div>
            {errors.password && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
  {errors.password}
</div>}
            <div className="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
         
              <input className="input100" value={data.password} type="password" name="password"  onChange={this.handleChange} placeholder="Password" />
              <span className="focus-input100"></span>
              <span className="symbol-input100">
                <span className="lnr lnr-lock"></span>
              </span>
            </div>
  
            <div className="contact100-form-checkbox m-l-4">
              <input className="input-checkbox100" id="ckb1" type="checkbox" name="remember-me" />
              <label className="label-checkbox100" htmlFor="ckb1">
                Remember me
              </label>
            </div>
            
            <div className="container-login100-form-btn p-t-25">
              <button className="login100-form-btn disabled" >
                Login
              </button>
            </div>
  
            <div className="text-center w-full p-t-42 p-b-22">
              <span className="txt1">
                Or login with
              </span>
            </div>
  
            <a href="#" className="btn-face m-b-10">
              <i className="fa fa-facebook-official"></i>
              Facebook
            </a>
  
            <a href="#" className="btn-google m-b-10">
              <img src={img} alt="GOOGLE" />
              Google
            </a>
  
            <div className="text-center w-full p-t-115">
              <span className="txt1">
                Not a member?
              </span>
  
              <Link className="txt1 bo1 hov1" to="/register">
                Sign up now							
              </Link>
            </div>
          </form>
        </div>
      </div>
       </div>
     );
}
}
const mapStateToProps = (state) =>{
  return state;
  
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(Login);
  