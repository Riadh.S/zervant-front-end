import React from 'react';
import Joi from 'joi-browser';
import Form from '../../commons/Form';
import './css/util.css';
import './css/main.css';
import './vendor/bootstrap/css/bootstrap.min.css';
import './images/icons/favicon.ico';
import '../../assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css';
import './vendor/select2/select2.min.css';
import '../../assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css';
import './vendor/animate/animate.css';
import './vendor/css-hamburgers/hamburgers.min.css';
import {userActions} from '../../_actions';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../../_helpers';
class Register extends Form{
  state = { 
    data:{
      name:'',
      email:'',
      password:'',
      companyName:'',   
    },
    errors:'',
 }
 schema ={ 
  name: Joi.string().required(),   
    email: Joi.string().email().required(),    
    password: Joi.string().min(8).required(),
    companyName: Joi.string().required()
}
componentWillMount(){
  console.log(this.props);
  if(localStorage.getItem('auth')){
      history.push('/home');
}
}
async doSubmit(){
  if(!this.state.errors){
    
   this.register();
      
}else{
  const errors='';
  this.setState({errors});
}
}
register(){
  const user={
    name:this.state.data.name,
    email:this.state.data.email,
    password:this.state.data.password,
    companyName:this.state.data.companyName
  };
  const {dispatch}=this.props;
  dispatch(userActions.register(user));
}
    render(){
        const {data,errors}=this.state;
      const {userResponse}=this.props.users;
        return (
      <div className="limiter">
      <div className="container-login100">
        <div className="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
          <form className="login100-form validate-form" onSubmit={this.handleSubmit}>
            <span className="login100-form-title p-b-55">
              Create account
            </span>
            {userResponse && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
 {userResponse}
</div>}          
{errors.name && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
 {errors.name}
</div>}
            <div className="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
              <input className="input100" type="text" value={data.name} name="name" onChange={this.handleChange} placeholder="User name" />
              <span className="focus-input100"></span>
              <span className="symbol-input100">
                <span className="lnr lnr-user"></span>
              </span>
            </div>
            {errors.email && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
 {errors.email}
</div>}
            <div className="wrap-input100 validate-input m-b-16" data-validate = "Valid email is required: ex@abc.xyz">
              <input className="input100" type="text" value={data.email} onChange={this.handleChange} name="email" placeholder="Email" />
              <span className="focus-input100"></span>
              <span className="symbol-input100">
                <span className="lnr lnr-envelope"></span>
              </span>
            </div>
            {errors.password && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
 {errors.password}
</div>}
            <div className="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
              <input className="input100" type="password" value={data.password} name="password" onChange={this.handleChange} placeholder="Password" />
              <span className="focus-input100"></span>
              <span className="symbol-input100">
                <span className="lnr lnr-lock"></span>
              </span>
            </div>
            {errors.companyName && <div class="wrap-input100 alert alert-danger m-b-16" role="alert">
 {errors.companyName}
</div>}
            <div className="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
              <input className="input100" type="text" value={data.companyName} onChange={this.handleChange} name="companyName" placeholder="Company" />
              <span className="focus-input100"></span>
              <span className="symbol-input100">
                <span className="lnr lnr-apartment"></span>
              </span>
            </div>
            
            <div className="contact100-form-checkbox m-l-4">
              <input className="input-checkbox100" id="ckb1" type="checkbox" name="remember-me" />
              <label className="label-checkbox100" for="ckb1">  
                Remember me
              </label>
            </div>
            
            <div className="container-login100-form-btn p-t-25">
              <button className="login100-form-btn">
                Register
              </button>
            </div>
  
          
            <div className="text-center w-full p-t-115">
              <span className="txt1">
                Not a member?
              </span>
  
              <Link className="txt1 bo1 hov1" to="/login">
                Sign in							
              </Link>
            </div>
          </form>
        </div>
      </div>
       </div>
     );
}
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(Register);