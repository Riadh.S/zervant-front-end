import React from 'react';
import {Modal,Button} from 'react-bootstrap';
import Form from '../../commons/Form';
import Joi from 'joi-browser';
import {Redirect} from 'react-router-dom'; 
import { getIdUser } from '../../_services/UserServices';
import Input from '../Input';
import { connect } from 'react-redux';
import { productActions } from '../../_actions';
import Select from '../../commons/Select';
class AddProduct extends Form {
    constructor(props) {
        super(props);
        this.state = {    
          errors:''   
         }
    }
    schema ={ 
        name: Joi.string().required(),   
          description: Joi.string().required(),    
          unit: Joi.string().required(),
            priceWT: Joi.required(),   
              vat: Joi.required(),    
          
      }
      validate=()=>{
        const {data}=this.props.product;
        const product={
          name:data.name,
          description:data.description,
          unit:data.unit,
          priceWT:data.priceWT,
          vat:data.vat
        }
        const resultat=Joi.validate(product,this.schema,{abortEarly:false});
        if(!resultat.error) return null;
        const errors={};
         for(let item of resultat.error.details)
         errors[item.path[0]]=item.message;
        return errors;
       }  
        handleSubmit=(e)=>{
          e.preventDefault();
          const {data}=this.props.product;
          console.log(data)
     const errors=this.validate();
     this.setState({errors:errors || {}});
     if (errors) return;
     this.doSubmit();        
    }
    calculateTTC=()=>{
      const {data}=this.props.product;
      let vat=1+(data.vat/100);
      console.log(vat);
      let priceTTC=data.priceWT*vat;
      return priceTTC;
    }
      handleChange=(e)=>{
        const {dispatch}=this.props;
        dispatch(productActions.changeDataProduct(e.target.name,e.target.value));
      }
     
       doSubmit(){  
        const {data}=this.props.product;
        console.log(data);
        if(!this.state.errors){
         
          const product={
            id_user:getIdUser(),
            name:data.name,
            description:data.description,
            unit:data.unit,
            priceBasedOn:data.priceBasedOn,
            priceWT:data.priceWT,
            vat:data.vat,
            priceTTC:this.calculateTTC()
          }
          console.log(product);
          const {dispatch}=this.props;
          dispatch(productActions.addProduct(product)); 
      
    }else{
      const errors='';
      this.setState({errors});
    }
  } 
    render() {
        const {data,product}=this.props.product; 
        const {status,onHandleClose}=this.props;
        const {errors}=this.state;
        console.log(data.priceBasedOn);
        return (  
            <Modal show={status} onHide={onHandleClose}>
            <Modal.Header className="bg-info" closeButton>
              <Modal.Title>Add new product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="row">
       <div className="col-lg-8"> 
        <form onSubmit={this.handleSubmit}>
        {product && <div className="wrap-input100 alert alert-danger m-b-16" role="alert">
{product}
</div>}  
<Input value={data.name}  type="text" name="name" onHandleChange={this.handleChange}
 label="name" error={errors.name}/>
    <Input value={data.description}  type="text" name="description" onHandleChange={this.handleChange}
 label="description" error={errors.description}/>
    <Input value={data.unit}  type="text" name="unit" onHandleChange={this.handleChange}
 label="unit" error={errors.unit}/>
 <Select value={["price including VAT","price excluding VAT"]} name="priceBasedOn" onHandleSelect={this.handleChange}
 label="Price based on"/>
    <Input value={data.priceWT}  type="text" name="priceWT" onHandleChange={this.handleChange}
 label="priceWT" error={errors.priceWT}/>
    <Input value={data.vat}  type="text" name="vat" onHandleChange={this.handleChange}
 label="VAT" error={errors.vat}/>
    <Input value={this.calculateTTC()}  type="text" name="priceTTC" 
 label="price TTC"/>
  {this.buttonSubmit("Submit")}
</form>
</div>
</div> 
            </Modal.Body>
            <Modal.Footer className="bg-info">
              <Button variant="secondary" onClick={onHandleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal> 
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(AddProduct);