import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import AddProduct from './AddProduct';
import SearchProduct from './SearchProduct';
import DetailProduct from './DetailProduct';
import _ from 'lodash';
import Pagination from '../../commons/Pagination';
import { connect } from 'react-redux';
import { productActions } from '../../_actions';
class Products extends Component {
  constructor(props) {
    super(props);
  this.state = { 
        show:false,
        order:1
     }
    }
     componentWillMount(){ 
      const { dispatch } = this.props;
      dispatch(productActions.getProducts());
}
    handleShow=()=>{
      this.setState({show:true});
    }
    handleChange=(nbPage)=>{
      console.log(nbPage);
      const {dispatch}=this.props;
      dispatch(productActions.productPagination(nbPage));
     }
    handleClose=()=>{
      this.setState({show:false});
    }
   
  handleDetailProduct=(product)=>{
    console.log(product);
    const {dispatch}=this.props;
    dispatch(productActions.detailProduct(product));
  }
  handleDelete=(id)=>{
    const products=products.filter(product =>product._id!==id);
    this.setState({products,detailProduct:''});
   /* const result=deleteProduct(id);
    console.log(result);*/
  }
 handleAdd=(product)=>{
    console.log(product);
    products.push(product);
    const products=products;
    this.setState({products});
  }
  handleOrder=(val)=>{
    let order=0;
    if(this.state.order===1){
      order=2;
    }
    if(this.state.order===2){
      order=1;
    }
    this.setState({order});
    const {dispatch}=this.props;
    dispatch(productActions.orderProducts(val,order));
   }
     render() {
       console.log(this.props.product);
       const {products,searchData,load,detailProduct,listPage,currentPage}=this.props.product;
      const productsData=(searchData!=="") ? products.filter(product =>(product.name.startsWith(searchData))) :products;
        const end=currentPage*listPage;
        const start=(end-listPage);
        console.log(start);
        console.log(end);
        let MyProducts=[];
        productsData ? MyProducts=productsData.slice(start,end) : MyProducts=productsData;
        console.log(load);
        
      return (
          <div>
          <div className="row m-t-8">
          <div className="col-sm-6">
          <section className="content">
          <div className="box">
      <div className="box-header">
        <div className="row">
            <div className="col-sm-4">
              <h3 className="box-title m-t-6">List of products</h3>
            </div>
            <div className="col-sm-4">
           <SearchProduct />
            </div>
            <div className="col-sm-2 m-r-12">
            <Button className="btn btn-primary m-t-6" onClick={this.handleShow}>Add new product</Button> 
            </div>
        </div>
      </div>
      <div className="box-body m-t-6">
            <table className="table">
            <thead>
              <tr>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("name")}>Name</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("description")}>description</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("priceWT")}>Price</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("vat")}>VAT</th>
               </tr>
            </thead>
            <thead>
          
{load===true && <h1>loading ...</h1>}   
   
             {MyProducts && MyProducts.map(product => 
              <tr key={product.id} onClick={() =>this.handleDetailProduct(product)}>
                   <th className="text-secondary">{product.name}</th>
                   <th className="text-secondary">{product.description}</th>
                   <th className="text-secondary">{product.priceWT}</th>
                   <th className="text-secondary">{product.vat}%</th>
              </tr>
            )}
            </thead>
          
          </table>
          </div></div>
          </section>
        {MyProducts && <Pagination length={productsData.length} 
static={currentPage} 
onChange={this.handleChange} 
             nbrPage={listPage}/>}       
          </div>
        <div className="col-md-6 col-md-offset-2 bg-light">
            <div className="panel panel-default">
                <div className="panel-body">
{detailProduct && <DetailProduct />}
{!detailProduct &&
<div className="card text-center">
  <div className="card-header">
    Product
  </div>
  <div className="card-body">
    <h5 className="card-title">Select Product</h5>
    <p className="card-text">select one of list products for more details .</p>
   <br/><br/>
    <a href="#!" className="btn btn-primary">import products</a>
  <br/><br/>
  </div>
  <div className="card-footer text-muted">
  
  </div>
</div>
}
        </div></div></div></div>
        <AddProduct status={this.state.show} onHandleClose={this.handleClose} 
        onHandleAdd={this.handleAdd}/>  
</div>
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(Products);