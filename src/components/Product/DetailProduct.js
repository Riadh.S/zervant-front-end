import React, { Component } from 'react';
import {Button,Modal} from 'react-bootstrap';
import Form from '../../commons/Form';
import Joi from 'joi-browser';
import {getIdUser} from '../../_services/UserServices';
import { connect } from 'react-redux';
import { productActions } from '../../_actions';
import Input from '../Input';
import Select from '../../commons/Select';
class DetailProduct extends Form {
    constructor(props) {
        super(props);
        this.state = {
          show:false,
        errors:'',
        deleteProduct:false 
         }
    }
    schema ={ 
      name: Joi.string().required(),   
        description: Joi.string().required(),    
        unit: Joi.string().required(),
        priceBasedOn: Joi.string().required(),
          priceWT: Joi.required(),   
            vat: Joi.required(),    
            priceTTC: Joi.required()
    }
   handleCloseDelete=()=>{
     this.setState({deleteProduct:false});
   } 
   calculateTTC=()=>{
    const {detailProduct}=this.props.product;
    let vat=1+(detailProduct.vat/100);
    console.log(vat);
    let priceTTC=detailProduct.priceWT*vat;
    return priceTTC;
  }
   handleShowDelete=()=>{
    this.setState({deleteProduct:true});
   }
   validate=()=>{
    const resultat=Joi.validate(this.props.product.DetailProduct,this.schema,{abortEarly:false});
    if(!resultat.error) return null;
    const errors={};
     for(let item of resultat.error.details)
     errors[item.path[0]]=item.message;
    return errors;
   }  
    handleSubmit=(e)=>{
      e.preventDefault();
 const errors=this.validate();
 this.setState({errors:errors || {}});
 if (errors) return;
 this.doSubmit();        
}
  handleChange=(e)=>{
    const {dispatch}=this.props;
    dispatch(productActions.changeValueProduct(e.target.name,e.target.value));
  }
  doSubmit(){
      if(!this.state.errors){
         this.putProduct();
         }
}
putProduct() {
const {detailProduct}=this.props.product;
const product={
  _id:detailProduct._id,
  id_user:getIdUser(),
  name:detailProduct.name,
  description:detailProduct.description,
  unit:detailProduct.unit,
  priceBasedOn:detailProduct.priceBasedOn,
  priceWT:detailProduct.priceWT,
  vat:detailProduct.vat,
  priceTTC:this.calculateTTC()
}
const id=detailProduct._id;
const {dispatch}=this.props;
dispatch(productActions.updateProduct(id,product));  
}
handleDelete=(id)=>{
  const {dispatch}=this.props;
  dispatch(productActions.deleteProduct(id));
  this.setState({deleteProduct:false});
}
   handleShow=()=>{
     /* const detailProduct={
        name:this.props.detailProductBase.name,
        description:this.props.detailProductBase.description,
        unit:this.props.detailProductBase.unit,
        priceBasedOn:this.props.detailProductBase.priceBasedOn,
        priceWT:this.props.detailProductBase.priceWT,
        vat:this.props.detailProductBase.vat,
        priceTTC:this.props.detailProductBase.priceTTC
      }*/
      this.setState({show:true});
    }
    handleClose=()=>{
      this.setState({show:false});
    }
    render() {  
      const {product,detailProduct}=this.props.product; 
       const {errors}=this.state;
        return ( 
          <div className="card text-center m-l-8">
          <div className="card-header">
            Product
          </div>
          <div className="card-body">
            <h5 className="card-title">{detailProduct.name}</h5>
            <p className="card-text">unit : {detailProduct.unit}</p>
            <p className="card-text">Price based on : {detailProduct.priceBasedOn}</p>
            <p className="card-text">Price HT : {detailProduct.priceWT}</p>
            <p className="card-text">VAT : {detailProduct.vat}</p>
            <p className="card-text">Price TTC : {detailProduct.priceTTC}</p>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
          </div>
          <div className="card-footer text-muted">
            <Button className="btn btn-info m-l-4" onClick={this.handleShow}>update</Button>
            <Button className="btn btn-danger m-l-4" onClick={this.handleShowDelete}>delete</Button>
          </div>
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header className="bg-info" closeButton>
              <Modal.Title>Update product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="row">
       <div className="col-lg-8"> 
        <form onSubmit={this.handleSubmit}>
        {product && <div className="wrap-input100 alert alert-danger m-b-16" role="alert">
{product}
</div>}   
 
  <Input value={detailProduct.name}  type="text" name="name" onHandleChange={this.handleChange}
 label="name" error={errors.name}/>
    <Input value={detailProduct.description}  type="text" name="description" onHandleChange={this.handleChange}
 label="description" error={errors.description}/>
    <Input value={detailProduct.unit}  type="text" name="unit" onHandleChange={this.handleChange}
 label="unit" error={errors.unit}/>
    <Select value={["price including VAT","price excluding VAT"]} name="priceBasedOn" onHandleSelect={this.handleChange}
 label="Price based on" />
    <Input value={detailProduct.priceWT}  type="text" name="priceWT" onHandleChange={this.handleChange}
 label="priceWT" error={errors.priceWT}/>
    <Input value={detailProduct.vat}  type="text" name="vat" onHandleChange={this.handleChange}
 label="VAT" error={errors.vat}/>
    <Input value={this.calculateTTC()}  type="text" name="priceTTC"
 label="price TTC" />
 {this.buttonSubmit("Update")}
</form>
</div>
</div> 
         </Modal.Body>
            <Modal.Footer className="bg-info">
              <Button variant="secondary" onClick={this.handleCloseDelete}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal show={this.state.deleteProduct}  onHide={this.handleCloseDelete}>
          <Modal.Header className="bg-info" closeButton>
            <Modal.Title>Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are You sure!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseDelete}>
              Close
            </Button>
            <Button variant="primary" onClick={()=>this.handleDelete(detailProduct._id)}>
              Confirm
            </Button>
          </Modal.Footer>
        </Modal> 
        </div>
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(DetailProduct);
