import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Navbar from '../components/NavBar';
export const PrivateRoute = ({ component: Component, ...rest }) => (
   <div>
   <Navbar/>
   
   <Route {...rest} render={props => (
       localStorage.getItem('auth') 
       ? <Component {...props} /> 
       : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
   
       )} />
       </div>

)