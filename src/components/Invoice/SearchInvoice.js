import React, { Component } from 'react';
import { invoiceActions } from '../../_actions';
import { connect } from 'react-redux';
class SearchInvoice extends Component {
  constructor(props){
    super(props);
  }
  handleSearch=(e)=>{  
   const searched=e.target.value;
   const {dispatch}=this.props;
   dispatch(invoiceActions.searchInvoice(searched));
   }
  
    render() {
      const {searchData}=this.props.invoice; 
        return ( 
            <form>
  <div className="form-row">
    <div className="form-group col-md-12 m-t-6">
   
       <input type="text" value={searchData} className="form-control" onChange={(e) =>this.handleSearch(e)} placeholder="Search"/>   
    </div>
  </div></form>
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(SearchInvoice);

         
         