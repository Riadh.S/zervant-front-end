import React, { Component } from 'react';
import {billSettingActions, invoiceActions,customerActions,productActions,paymentActions} from '../../_actions';
import {connect} from 'react-redux';
import Textaria from '../../commons/Textaria';
import Select from '../../commons/Select';
import DatePicker from "react-datepicker";
import {Link} from 'react-router-dom';
import "react-datepicker/dist/react-datepicker.css";
import _ from 'lodash';
import { getIdUser } from '../../_services/UserServices';
class AddInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
          startDate1: new Date(),
          startDate2: new Date(),
          show:true,
          length:1,
          table:[0]
        }
    }
    handleChangeDate1=(date) =>{
        this.setState({
          startDate1: date
        });
      }
      handleChangeDate2=(date) =>{
        this.setState({
          startDate2: date
        });
      }
      handleChangeDate=(date,val) =>{
        console.log(val);
        const {dispatch}=this.props;
        dispatch(invoiceActions.changeDate(date,val));
        
      }
      handleChange=(e)=>{
        const {dispatch}=this.props;
        dispatch(invoiceActions.changeDataInvoice(e.target.name,e.target.value));
      }
    componentWillMount(){
        const {dispatch}=this.props;
  dispatch(billSettingActions.getInformationBillSetting());
  dispatch(productActions.getProducts());
  dispatch(paymentActions.getInformationPayment()); 
  dispatch(invoiceActions.getInvoices());
  dispatch(customerActions.getCustomers());
}
    
    handleShow=(val)=>{
      const tab=this.state.table.filter(nb=>nb!==val);
      console.log(tab);
      const lgt=tab.length;
       this.setState({length:lgt,table:tab});
    }
  handleAdd=()=>{
    const {dispatch}=this.props;
    dispatch(invoiceActions.addRowProduct());
    const length=this.state.length+1;
   const table=[];
   for(let i=0;i<length;i++){
    table.push(i);
}

console.log(length);
console.log(table);
this.setState({length:length,table:table});
console.log(this.state);

  }
  handleChangeProducts=(e,val,products)=>{
    console.log(products);
    const {dispatch}=this.props;
        dispatch(invoiceActions.changeDataInvoiceProducts(e.target.name,e.target.value,val,products));
  }
calculeDate=()=>{
  const {data}=this.props.billSetting;
  const {startDate1}=this.state;
  let dd = startDate1.getDate()+data.paymentCondition.valuePD;
  let mm=startDate1.getMonth()+1;
  let y= startDate1.getFullYear();
  if(dd>30){
    const mt=(dd/30)+1;
    dd=dd%30;
    mm = startDate1.getMonth()+parseInt(mt);
  }
  if(mm>12){
    const ym=(mm/12);
    mm=mm%12;
    y= startDate1.getFullYear()+parseInt(ym);
  }

  var someFormattedDate = mm + '/' + dd + '/' + y;
  return someFormattedDate;
}
submit=()=>{
  const {dataInvoice,invoices}=this.props.invoice;
  const {data}=this.props.payment;
  const {customersList}=this.props.customers;
 let idClient=0;
  {customersList && customersList.map((client)=>{
    if(client.companyName===dataInvoice.companyName){
      idClient=client._id;
    }
    }
    )};
 const dataPdf={
      id_user:getIdUser(),
      id_client:idClient,
      billNumber:3, 
     billDate:this.state.startDate1,
     deadline:this.calculeDate(),
     companyName:dataInvoice.companyName,
     status:"created",
     message:dataInvoice.message,
     notesFooter:dataInvoice.notesFooter,
     products:dataInvoice.products,
     net:dataInvoice.net,
     totalTTC:dataInvoice.totalTTC,
     payment:data
  }
  console.log(dataPdf);
  const {dispatch}=this.props;
  dispatch(invoiceActions.generatePdf(dataPdf));
}

    render() {
      console.log(this.state);
        const {data}=this.props.billSetting; 
        const {dataInvoice,invoices}=this.props.invoice;
        const {products}=this.props.product;
        const {customersList}=this.props.customers;
        invoices && console.log(invoices.length);
        console.log(dataInvoice);
        return ( 
            <div className="bg-light">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <a className="navbar-brand" href="#">New invoice</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
     
    </ul>
    <form className="form-inline my-2 my-lg-0">
    <Link className="btn btn-secondary my-2 m-l-10" to="/invoice">Cancel</Link>
      <Link className="btn btn-info my-2 my-sm-0 m-l-10" to="/showpdf">Save as draft</Link>
      <Link className="btn btn-success my-2 my-sm-0 m-l-10" onClick={this.submit} to="/showpdf">next</Link>
    </form>
  </div>
</nav>
<div className="row">
<div className="col-10 m-t-30">
<div className="card text-center m-l-8">
          
          <div className="card-body">
          <div className="row">
 {customersList &&         
<div className="col-5 m-t-30">
<div className="col-2"><label>Customer</label></div>
<div className="input-group">
  <select className="form-control" name="companyName"  onChange={(e)=>this.handleChange(e)}  >
  {customersList.map((customer)=>
    <option>{customer.companyName}</option>
    )}
</select>
  </div>
  </div>
 }
<div className="col-3 m-t-30">
</div>
<div className="col-2 m-t-30">
<h5>{data.billHeader}</h5>
</div>
</div>
<hr></hr>

<div className="form-inline">
        <div className="col-4"><label >Invoice number</label></div>
        <div className="col-3">
        <label>Invoice 2</label>
        </div>
        {data.attentionOf===true &&
        <div className="col-3">
        <label >PO number</label><br></br>
          <input type="text" onChange={(e)=>this.handleChange(e)} className="form-control"/>
          </div>
        }
      </div>
      <div className="form-inline">
        <div className="col-4"><label >Date of invoice</label></div>
        <div className="col-3">
        <div className="col-md-6">
                                <div className="input-group date">
                                    <div className="input-group-addon">
                                        <i className="fa fa-calendar"></i>
                                    </div>
                                    <DatePicker
    selected={this.state.startDate1}
    onChange={this.handleChangeDate1}
/>
                                </div>
                            </div>
        </div>
        <div className="col-3">
        <label >message</label><br></br>
        <Textaria value={dataInvoice.message} name="message" onHandleChange={this.handleChange}/>
          </div>
      </div>
      {data.paymentCondition.displayPD===true && 
       <div>
       <div className="form-inline">
     
        <div className="col-4"><label >Payment terms</label></div>
        <div className="col-3">
        <div className="col-md-6">                         
                                    <Select value={["0","7","14","30","60","90"]} name="paymentCondition" onHandleSelect={this.handleChange}/>

                            </div>
        </div>
      </div>
      <div className="form-inline">
        <div className="col-4"><label >Due date</label></div>
      <div className="col-3">
        <div className="col-md-6">
                                <div className="input-group date">
                                   
                                    <label>{this.calculeDate()}</label>

                                </div>
                            </div>
        </div>
        </div>
        </div>}
        {data.paymentCondition.displayPD===false && 
        <div className="form-inline">
         <div className="col-4"><label >Due date</label></div>
        <div className="col-3">
        <div className="col-md-6">
                                <div className="input-group date">
                                   
                                <div className="input-group-addon">
                                        <i className="fa fa-calendar"></i>
                                    </div>
                                    <DatePicker
    selected={this.state.startDate2}
    onChange={this.handleChangeDate2}
/>

                                </div>
                            </div>
       </div>
        </div>}

      {data.delayInterest.displayDI===true && <div><div className="form-inline">
        <div className="col-4"><label >Late payment interest</label></div>
         <div className="col-3">
        <div className="col-md-6">
                                
                                   
                                <Select value={["0","8"]} name="valueDI" onHandleSelect={this.handleChange}/>

                            </div>
        </div>
</div>
<br></br>
<div className="form-inline">
        <div className="col-4"><label >Reference number</label></div>
         <div className="col-3">
        
                                <div className="form-inline">
                                   
                                <input value={data.delayInterest.valueDI} className="form-control" type="text" name="valueDI" onChange={(e)=>this.handleChange(e)}
 />

                                </div>
                            </div>
      
</div>
</div>}
<hr></hr>
{this.state.table && <div>
  {this.state.table.map((val)=>
<div className="row">
{products &&
<div className="col-2"><label className="text-info">Name product</label>
<div className="input-group">
  <select className="form-control" name="name" onChange={(e)=>this.handleChangeProducts(e,val,products)} >
  { products.map((product)=>
    <option>{product.name}</option>
  )}
  </select>
</div></div>}
{data.displayColunms[0].value===true &&
<div className="col-2"><label className="text-info">Date</label>
<div className="input-group date">
                                   
                                   <div className="input-group-addon">
                                           <i className="fa fa-calendar"></i>
                                       </div>
                                       <DatePicker
       selected={dataInvoice.products[val].date}
       onChange={(date)=>this.handleChangeDate(date,val)}
   />
   
                                   </div></div>}
 {data.displayColunms[1].value===true &&                                  
<div className="col-1"><label className="text-info">Qty</label>
                         
<input  className="form-control" type="text" name="quatity" value={dataInvoice.products[val].quatity} onChange={(e)=>this.handleChangeProducts(e,val)}
 />

</div>}
{data.displayColunms[2].value===true &&
<div className="col-1"><label className="text-info">unit</label>
                         
<input  className="form-control" type="text" name="unit" value={dataInvoice.products[val].unit} onChange={(e)=>this.handleChangeProducts(e,val)}
 />

</div>}
{data.displayColunms[3].value===true &&
<div className="col-2"><label className="text-info">unit Price</label>
                         
<input  className="form-control" type="text" name="priceUnit" value={dataInvoice.products[val].priceUnit} onChange={(e)=>this.handleChangeProducts(e,val)}
 />
</div>}
{data.displayColunms[4].value===true &&
<div className="col-1"><label className="text-info">VAT %</label>
                         
<input  className="form-control" type="text" name="vat" value={dataInvoice.products[val].vat} onChange={(e)=>this.handleChangeProducts(e,val)}
 />
</div>}
{data.displayColunms[5].value===true &&
  <div className="col-1"><label className="text-info">VAT TOTAL</label>
                           
  <input  className="form-control" type="text" name="price" value={dataInvoice.products[val].price} onChange={(e)=>this.handleChangeProducts(e,val)}
   />
  </div>}
{data.displayColunms[6].value===true &&
<div className="col-2"><label className="text-info">Total</label>
                         
<input  className="form-control" type="text" name="priceHT" value={dataInvoice.products[val].priceHT} onChange={(e)=>this.handleChangeProducts(e,val)}
 />

</div>}
<div className="col-1 m-t-500">
<br></br>
<span className="lnr lnr-cross-circle " onClick={()=>this.handleShow(val)}></span></div>
</div>

)}
</div>
}
<br></br>
<br></br>
<div className="row">
<div className="col-2">
<button className="btn btn-info" type="submit" onClick={this.handleAdd}>Add Item</button>
   </div>
  <div className="col-4"></div> 
  <div className="col-4">
  <hr></hr>
    <label>Net total </label><label className="m-l-220">{dataInvoice.net}</label><br></br>
    <hr></hr>
    <label className="text-dark">Total amount due </label><label className="m-l-150">{dataInvoice.totalTTC}</label>
 </div>
   
  
  </div>
   </div>
   <br></br>
<br></br>
<div className="row">
<div className="col-2">
<label >invoice footnote</label><br></br>
        <Textaria value={dataInvoice.notesFooter} name="notesFooter" onHandleChange={this.handleChange}/>
   </div></div>
          </div>
          <div className="card-footer text-muted">
           
</div>
</div> 
</div>

        </div> 
        
      
         );
    }
}
const mapStateToProps = (state) =>{
    return state;
  }
  export default connect(mapStateToProps, null, null, {
    pure: false
  })(AddInvoice);
