import React, { Component } from 'react';
import {Button,Modal} from 'react-bootstrap';
import Form from '../../commons/Form';
import {getIdUser} from '../../_services/UserServices';
import { connect } from 'react-redux';
import { invoiceActions } from '../../_actions';
import Input from '../Input';
import SelectInput from '../../commons/SelectInput';
import { PDFReader } from 'react-read-pdf';
class DetailInvoice extends Form {
    constructor(props) {
        super(props);
        this.state = {
          show:false,
        deleteProduct:false,
        numPages: null,
    pageNumber: 1 
         }
    }
   
   handleCloseDelete=()=>{
     this.setState({deleteProduct:false});
   } 
   
   handleShowDelete=()=>{
    this.setState({deleteProduct:true});
   }
   
handleDelete=(id)=>{
  const {dispatch}=this.props;
  dispatch(invoiceActions.deleteInvoice(id));
  this.setState({deleteProduct:false});
}
onDocumentLoadSuccess ({ numPages })  {
    this.setState({ numPages });
  }
   handleShow=()=>{
     this.setState({show:true});
    }
    handleClose=()=>{
      this.setState({show:false});
    }
    render() {  
      const {detailInvoice}=this.props.invoice;
      const { pageNumber, numPages } = this.state; 
        return ( 
          <div className="card text-center m-l-8">
          <div className="card-header">
            Product
          </div>
          <div className="card-body">
            <h5 className="card-title">{detailInvoice.companyName}</h5>
            <p className="card-text">Price HT : {detailInvoice.products[0].priceHT}</p>
            <p className="card-text">VAT : {detailInvoice.products[0].vat}</p>
            <p className="card-text">TOTAL : {detailInvoice.totalTTC}</p>
           <hr></hr>
            <p className="card-text">created : {detailInvoice.billDate}</p>
            <p className="card-text">Due date : {detailInvoice.billDate}</p>
            <br/>
            <hr></hr>
            
            <br/>
            <br/>
            <br/>
            <br/>
          </div>
          <div className="card-footer text-muted">
            <Button className="btn btn-info m-l-4" onClick={()=>this.handleShow(detailInvoice)}>show</Button>
            <Button className="btn btn-danger m-l-4" onClick={this.handleShowDelete}>delete</Button>
          </div>
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header className="bg-info" closeButton>
              <Modal.Title>Update product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div>
       <PDFReader url={`http://127.0.0.1:8001/api/bills/fetch-pdf/${detailInvoice._id}`} width="400"/>
      </div>
         </Modal.Body>
            <Modal.Footer className="bg-info">
              <Button variant="secondary" onClick={this.handleCloseDelete}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal show={this.state.deleteProduct}  onHide={this.handleCloseDelete}>
          <Modal.Header className="bg-info" closeButton>
            <Modal.Title>Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are You sure!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleCloseDelete}>
              Close
            </Button>
            <Button variant="primary" onClick={()=>this.handleDelete(detailInvoice._id)}>
              Confirm
            </Button>
          </Modal.Footer>
        </Modal> 
        </div>
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(DetailInvoice);
