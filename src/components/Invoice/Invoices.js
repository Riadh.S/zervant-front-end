import React, { Component } from 'react';
import SearchInvoice from './SearchInvoice';
import DetailInvoice from './DetailInvoice';
import _ from 'lodash';
import Pagination from '../../commons/Pagination';
import { connect } from 'react-redux';
import { invoiceActions ,billSettingActions} from '../../_actions';
import {Link} from 'react-router-dom';
class Invoices extends Component {
    constructor(props) {
        super(props);
      this.state = { 
            show:false
         }
        }
      componentWillMount(){
      const { dispatch } = this.props;
       dispatch(invoiceActions.getInvoices());
       dispatch(billSettingActions.getInformationBillSetting());
}
    handleShow=()=>{
      const { dispatch } = this.props;
      dispatch(invoiceActions.getInvoices());
      this.setState({show:true});
    }
    newInvoice=()=>{
      console.log("errorrrr");
     const {data}=this.props.billSetting;
     const { dispatch } = this.props;
       dispatch(invoiceActions.newInvoice(data));

    }
    handleChange=(nbPage)=>{
      console.log(nbPage);
      const {dispatch}=this.props;
      dispatch(invoiceActions.invoicePagination(nbPage));
     }
    handleClose=()=>{
      this.setState({show:false});
    }
   
    handleDetailInvoice=(invoice)=>{
    console.log(invoice);
    const {dispatch}=this.props;
    dispatch(invoiceActions.detailInvoice(invoice));
  }
  handleDelete=(id)=>{
    const products=products.filter(product =>product._id!==id);
    this.setState({products,detailProduct:''});
   /* const result=deleteProduct(id);
    console.log(result);*/
  }
 handleAdd=(product)=>{
    console.log(product);
    products.push(product);
    const products=products;
    this.setState({products});
  }
  handleOrder=(val)=>{
    const {dispatch}=this.props;
    dispatch(invoiceActions.orderInvoices(val));
   }
     render() {
         console.log(this.props.invoice);
       const {invoices,load,searchInvoice,detailInvoice,listInvoice,currentPageInvoice}=this.props.invoice;
      const invoicesData=(searchInvoice!=="") ? invoices.filter(invoice =>(invoice.companyName.startsWith(searchInvoice))) :invoices;
        const end=currentPageInvoice*listInvoice;
        const start=(end-listInvoice);
        const MyInvoices=invoicesData;
        console.log(load);
      return (
          <div>
          <div className="row m-t-10">
          <div className="col-sm-7">
          <section className="content">
          <div className="box">
      <div className="box-header">
        <div className="row">
            <div className="col-sm-4">
              <h3 className="box-title m-t-6">List of invoices</h3>
            </div>
            <div className="col-sm-4">
           <SearchInvoice />
            </div>
            <div className="col-sm-2 m-r-12">
            <Link className="btn btn-primary m-t-6" onClick={this.newInvoice} to="/addNewInvoice">Add new invoice</Link> 
            </div>
        </div>
      </div>
      <div className="box-body m-t-7">
            <table className="table table-hover dataTable">
            <thead>
              <tr>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("billNumber")}>Number</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("companyName")}>Customer</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("billDate")}>Created</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("billDate")}>Due Date</th>
                <th scope="col" className="text-info" >Paid</th>
                <th scope="col" className="text-info" onClick={() =>this.handleOrder("totalTTC")}>TOTAL</th>
                <th scope="col" className="text-info" >Status</th>
               </tr>
            </thead>
            <thead>
           
{load===true && <h1>loading ...</h1>}  
  
             {invoices && invoices.map(invoice => 
              <tr key={invoice.id} onClick={() =>this.handleDetailInvoice(invoice)}>
                   <th className="text-secondary">{invoice.billNumber}</th>
                   <th className="text-secondary">{invoice.companyName}</th>
                   <th className="text-secondary">{invoice.billDate}</th>
                   <th className="text-secondary">{invoice.billDate}</th>
                   <th className="text-secondary"></th>
                   <th className="text-secondary">{invoice.totalTTC}</th>
                   <th className="text-secondary">created</th>

              </tr>
            )}
            </thead>
          
          </table>
          </div></div>
          </section>
       {invoices &&   <Pagination length={MyInvoices.length} 
static={currentPageInvoice} 
onChange={this.handleChange} 
             nbrPage={listInvoice}/>}       
          </div>
        <div className="col-md-5 col-md-offset-2 bg-light">
            <div className="panel panel-default">
                <div className="panel-body">
{detailInvoice && <DetailInvoice />}
{!detailInvoice &&
<div className="card text-center">
  <div className="card-header">
    Invoice
  </div>
  <div className="card-body">
    <h5 className="card-title">Select Invoice</h5>
    <p className="card-text">select one of list invoices for more details .</p>
   <br/><br/>
  <br/><br/>
  </div>
  <div className="card-footer text-muted">
  
  </div>
</div>
}
        </div></div></div></div>
        {/*<AddInvoice status={this.state.show} onHandleClose={this.handleClose} 
        onHandleAdd={this.handleAdd}/>*/}  
</div>
         );
    }
}
const mapStateToProps = (state) =>{
  return state;
}
export default connect(mapStateToProps, null, null, {
  pure: false
})(Invoices);