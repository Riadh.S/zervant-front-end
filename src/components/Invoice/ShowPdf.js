import React, { Component } from 'react';
import { PDFReader } from 'react-read-pdf';
import {Link} from 'react-router-dom';
import {Dropdown} from 'react-bootstrap';
import {invoiceActions} from '../../_actions';
import {connect} from 'react-redux';
import { getIdUser } from '../../_services/UserServices';
class ShowPdf extends Component {
    state = {  }
    componentWillMount(){
        const {dispatch}=this.props;
        dispatch(invoiceActions.getInvoices());
    }
    submitInvoice=()=>{
        const {dataInvoice}=this.props.invoice;
        const {dispatch}=this.props;
        dispatch(invoiceActions.addInvoice(dataInvoice));
    }
    render() {
        const {dataPdf}=this.props.invoice; 
         
        return ( 
<div className="bg-light">
<nav className="navbar navbar-expand-lg navbar-light bg-light">
  <a className="navbar-brand" href="#">Approve and send</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
     
    </ul>
    <form className="form-inline my-2 my-lg-0">
    <Link className="btn btn-secondary my-2 m-l-10" to="/invoice">Go back</Link>
      <Link className="btn btn-info my-2 my-sm-0 m-l-10 m-r-10" onClick={this.submitInvoice} to="/invoice">Save as draft</Link>
      <Dropdown>
  <Dropdown.Toggle variant="success" id="dropdown-basic">
    Approve and send
  </Dropdown.Toggle>
  <Dropdown.Menu>
    <Dropdown.Item href="#/action-1">By email</Dropdown.Item>
    <Dropdown.Item href="#/action-2">Save and Approve</Dropdown.Item>
    <Dropdown.Item href="#/action-3">Download PDF</Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
    </form>
  </div>
</nav>
        <PDFReader url={`http://127.0.0.1:8001/api/bills/fetch-pdf/${dataPdf._id}`} width="600"/>
       </div>
         );
    }
}
 
const mapStateToProps = (state) =>{
    return state;
  }
  export default connect(mapStateToProps, null, null, {
    pure: false
  })(ShowPdf);