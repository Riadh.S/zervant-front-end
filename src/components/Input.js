import React, { Component } from 'react';
class Input extends Component{
    
    render(){
    const {label,type,name,value,error}=this.props;
    return ( 
        <div className="form-group">
        <label htmlFor={name}>{label}</label>
        <input type={type} name={name} value={value} className="form-control" id={name} onChange={(e)=>this.props.onHandleChange(e)} />
         {error && <div className="alert alert-danger" role="alert">
      {error}
    </div>}
      </div>
     );
}
}
export default Input;