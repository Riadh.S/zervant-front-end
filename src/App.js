import React, { Component } from "react";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import Home from "./components/Home";
import NavBar from "./components/NavBar";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";
import Invoices from "./components/Invoice/Invoices";
import AddInvoice from "./components/Invoice/AddInvoice";
import ShowPdf from "./components/Invoice/ShowPdf";
import Estimation from "./components/Estimation";
import TechnicalSupport from "./components/TechnicalSupport";
import MyAccount from "./components/MyAccount/MyAccount";
import Customers from "./components/Customers/Customers";
import Products from "./components/Product/Products";
import NotFound from "./components/not-found";
import { PrivateRoute } from "./components/PrivateRoute";
import { history } from "./_helpers";

class App extends Component {
  state = {};
  render() {
    return (
      <div className="App">
        <Router history={history}>
          <Switch>
            <PrivateRoute path="/home" component={Home} />
            <PrivateRoute
              path="/technicalSupport"
              component={TechnicalSupport}
            />
            <PrivateRoute path="/estimation" component={Estimation} />
            <PrivateRoute path="/invoice" component={Invoices} />
            <PrivateRoute path="/account" component={MyAccount} />
            <PrivateRoute path="/customers" component={Customers} />
            <PrivateRoute path="/products" component={Products} />
            <PrivateRoute path="/addNewInvoice" component={AddInvoice} />
            <PrivateRoute path="/showpdf" component={ShowPdf} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/not-found" component={NotFound} />
            <Redirect from="/" exact to="/login" />
            <Redirect to="/not-found" />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
