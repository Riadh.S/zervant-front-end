import React from 'react';
const CheckBox = (props) => {
   const {name,value,label}=props;
    return ( 
      <div className="form-inline">
      <div className="col-2"></div>
      <div className="col-2"><label>
          {label}
      </label></div>
     <div className="col-3 custom-control custom-checkbox my-1 mr-sm-2">
      <input  type="checkbox" id="customControlInline" checked={value} name={name} onChange={()=>props.onHandleChange({value},{name})}/>  
  </div>
  </div>
     );
}
 
export default CheckBox;