import React, { Component } from 'react';
const SelectInput = (props) => {
    const {label,name,value}=props;
    return ( 
        <div className="form-inline">
        <div className="col-2"></div> <div className="col-2"><label>{label}</label></div>
        <div className="col-5">
        <select style={{width:"60%"}} className="form-control" name={name} onChange={(e) => props.onHandleSelect(e)}>
          {value.map((genre) => 
          <option>{genre}</option>
          )}
        </select>
        </div>
      </div>
     );
}
 
export default SelectInput;