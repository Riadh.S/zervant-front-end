import React, { Component } from 'react';
class InputNotRequired extends Component{
    
    render(){
    const {label,type,name,value}=this.props;
    return ( 
        <div className="form-inline">
        <div className="col-2"> </div><div className="col-2"><label htmlFor={name}>{label}</label></div>
        <div className="col-5">
        <input type={type} name={name} value={value} className="form-control" id={name} onChange={(e)=>this.props.onHandleChange(e)} />
        </div>
      </div>
     );
}
}
export default InputNotRequired;