import React, { Component } from 'react';
import Joi from 'joi-browser';
import Input from '../components/Input';

class Form extends Component {
    state = {
         data:{}, 
        errors:{}
     }
     validate=()=>{
        const resultat=Joi.validate(this.state.data,this.schema,{abortEarly:false});
        if(!resultat.error) return null;
        const errors={};
         for(let item of resultat.error.details)
         errors[item.path[0]]=item.message;
        return errors;
       }  
        handleSubmit=(e)=>{
          e.preventDefault();
     const errors=this.validate();
     this.setState({errors:errors || {}});
     if (errors) return;
     this.doSubmit();        
    }
      handleChange=(e)=>{
        const data={...this.state.data};
        data[e.target.name]=e.target.value;
          this.setState({data});
      }
   inputForm(value,type,name,label,error){
     return(
      <Input value={value}  type={type} name={name} onHandleChange={this.handleChange}
 label={label} error={error}/>
     );
   }

   buttonSubmit(label){
     return(
      <button type="submit" className="btn btn-primary">{label}</button>
     );
   }
}
 
export default Form;