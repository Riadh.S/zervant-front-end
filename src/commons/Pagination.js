import React, { Component } from 'react';

class Pagination extends Component {

    renderPagination = () => {
        const n=Math.ceil(this.props.length/this.props.nbrPage);
        const tab=[];
        for(let i=1;i<=n;i++){
            tab.push(i);
        }
        if(tab.length==1){
            return null;
        }
        return (
            <ul className="pagination">
       { tab.map(e =>
            <li key={e} className="page-item"><button key={e} className="btn btn-info" active={this.props.static==e ? "active" :""} onClick={() => this.props.onChange(e)}>{e}</button></li>
            )}
            </ul>)
    }
    render() {
        console.log(this.props);
        return (  
            <nav aria-label="Page navigation example">{this.renderPagination()}</nav>
        );
    }
}
 
export default Pagination;