import React, { Component } from 'react';
import InputNotRequired from './InputNotRequired';
import Textaria from './Textaria';
import SelectInput from './SelectInput';
import CheckBox from './CheckBox';
class FormNotRequired extends Component {
    state = { 
        data:{}
     }
        handleSubmit=(e)=>{
          e.preventDefault();
         this.doSubmit();        
    }
      handleChange=(e)=>{
        const data={...this.state.data};
        console.log(data[e.target.name]);
        data[e.target.name]=e.target.value;
          this.setState({data});
      }
      handleSelect=(e)=>{
        const data={...this.state.data};
        data[e.target.name]=e.target.value;
        console.log(e.target.value);
        this.setState({data});
      }
      handleCheck=(valeur,name)=>{
        const data={...this.state.data};
        const indice=name.name;
        if(valeur.value===true){
        data[indice]=false;
      }else{
        data[indice]=true;
      }
      console.log(data.valeur);
        this.setState({data});   
      }
   inputForm(value,type,name,label){
     return(
      <InputNotRequired value={value}  type={type} name={name} onHandleChange={this.handleChange}
 label={label}/>
     );
   }
   chexBoxForm(value,name,label){ 
    return (
     <CheckBox value={value} name={name} label={label} onHandleChange={this.handleCheck}/>
     );
    }
   textAriaForm(value,name,label){
    return(
     <Textaria value={value} name={name} onHandleChange={this.handleChange}
label={label}/>
    );
  }
  selectForm(value,name,label){
    return(
     <SelectInput value={value} name={name} onHandleSelect={this.handleSelect}
label={label}/>
    );
  }
   buttonSubmit(label){
     return(
      <button type="submit" className="btn btn-info">{label}</button>
     );
   }
} 
 
export default FormNotRequired;