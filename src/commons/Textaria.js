import React, { Component } from 'react';
const Textaria = (props) => {
    const {label,value,name}=props;
    return ( 
        <div class="mb-3 form-inline">
       <div className="col-2"></div> <div className="col-2"> <label for="validationTextarea">{label}</label></div>
       <div className="col-5"> 
        <textarea className="form-control" value={value} name={name} rows="5" onChange={(e)=>props.onHandleChange(e)}></textarea>
      </div>
      </div>
     );
}
 
export default Textaria;