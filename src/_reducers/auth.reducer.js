import { LOGIN_SUCCESS,LOGOUT_SUCCESS,LOGIN_FAILED} from "../_actions/types";
const initialState ={
token : '',
auth  :'',
userResponse:'',
loggingIn:false
}
export function authentication(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        loggingIn: true,
        auth: action.auth,
        token: action.token,
        userResponse:action.userResponse
      };
    case LOGOUT_SUCCESS:
      return {
        auth: false
      };
      case LOGIN_FAILED:
      return {
        loggingIn: false,
        auth: false,
        userResponse:action.userResponse
      };
    default:
      return state
  }
}