import { PAYMENT_INFORMATION_FOUND,PAYMENT_INFORMATION_NOTFOUND,ADD_PAYMENT,UPDATE_PAYMENT,CHANGE_DATA_PAYMENT} from "../_actions/types";
const initialState={
    data:{
        paymentMeans:'',
        paypal:'' ,
        bancData:{bancDATA:'',swiftCode:'',IBAN:''},
        RIB:{
            bancRIB:'',
            codeBox:'',
            accountNumber:'',
            accountHolder:''
        }   

    },
    found:false
}
export function payment(state=initialState,action){
    switch (action.type){
        case  PAYMENT_INFORMATION_FOUND:{
            return{
               data:action.data,
               found:true 
            }
        }
        case PAYMENT_INFORMATION_NOTFOUND:{
            return{
                data:{
                    paymentMeans:'',
                    paypal:'' ,
                    bancData:{bancDATA:'',swiftCode:'',IBAN:''},
                    RIB:{
                        bancRIB:'',
                        codeBox:'',
                        accountNumber:'',
                        accountHolder:''
                    }   
            
                },
                found:false  
            }
        }
    
    case CHANGE_DATA_PAYMENT:{
        const payment=state.data;
        if((action.name==="bancDATA")||(action.name==="swiftCode")||(action.name==="IBAN")){
        payment.bancData[action.name]=action.value;
        }
        else if((action.name==="bancRIB")||(action.name==="codeBox")||(action.name==="accountNumber")||((action.name==="accountHolder"))){
           payment.RIB[action.name]=action.value;
        }
        else{
        payment[action.name]=action.value;
        }
        return{
          ...state,
          data:payment
        }
      }
      case ADD_PAYMENT:{
          return {
              ...state,
              found:true
      }
    }
    case UPDATE_PAYMENT:{
   return state;
    }
    default:{
      return state;
}
}
}