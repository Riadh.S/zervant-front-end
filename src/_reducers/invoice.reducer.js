import {FETECHED_ALL_INVOICE,ID_PDF,CHANGE_DATE,ADD_INVOICE,ADD_ROW_PRODUCT,NEW_INVOICE,CHANGE_DATA_INVOICE_PRODUCTS,FETCH_START,ORDER_INVOICES,CHANGE_DATA_INVOICE,SEARCH_INVOICES,DELETE_INVOICE,DETAIL_INVOICES,PAGINATION_INVOICE,UPDATE_INVOICE,UPDATE_INVOICE_FAILED,CHANGE_VALUE_INVOICE} from '../_actions/types';
import _ from 'lodash';

const initialState={
  searchInvoice:'',
        detailInvoice:'',
        listInvoice:4,
        currentPageInvoice:1,
        dataPdf:'',      
     dataInvoice:{
        companyName: '',
        billNumber : 0,
        billDate : '',
        conditionPayment : 0,
        deadline :'',
        message : '',
        notesFooter : '',
        net:0,
        discount : 0,
        totalTTC : 0,
        products:[
          {
            id_product : '',
             name : '',
            date :new Date(),
            quatity : 1,
            unit : '',
            date :'',
            quatity : 0,
            unit : 0,
           priceUnit : 0,
           vat: 0,
           price : 0,
           priceHT : 0
          }
        ]
   },
   invoice:'',
   load:false,
   invoices:[]
  }
export function invoice(state = initialState, action) {
                switch (action.type) {
                  case FETCH_START:{
                    return{
                       ...state,
                       load:true
                    }
                  }
                  case FETECHED_ALL_INVOICE:{
                    console.log(action.invoices);  
                  
                    return {
                      ...state,
                      currentPageInvoice:1,
                      invoices:action.invoices,
                      load:false,
                      searchInvoice:''
                    }
                    }
                    case ID_PDF:{
                      return{
                        ...state,
                        dataPdf:action.invoice
                      }
                    }
                    case ORDER_INVOICES:{
                      const invoices=_.orderBy(state.invoices,action.valeur,"asc");
                      console.log(invoices);
                      return{
                        ...state,
                        invoices:invoices
                      }
                    }
                    case CHANGE_DATE:{
                      const product=state.dataInvoice;
                     product.products[action.val].date=action.date;
                     return{
                       ...state,
                       dataInvoice:product
                     } 
                    }
                    case SEARCH_INVOICES:{
                      return{
                        ...state,
                        searchData:action.name
                      }
                    }
                    case DETAIL_INVOICES:{
                      return{
                        ...state,
                        detailInvoice:action.invoice
                      }
                    }
                    case PAGINATION_INVOICE:{
                      console.log(action.nbrPage);
                      return {
                      ...state,
                      currentPage:action.nbrPage
                      
                    }
                  }
                  case NEW_INVOICE:{
                    console.log(action.data);
                    let invoice=state.dataInvoice;
                    invoice.message=action.data.message;
                    invoice.notesFooter=action.data.notesFooter;
                    return{
                      ...state,
                      dataInvoice:invoice
                    }
                  }
                  case UPDATE_INVOICE_FAILED:{
                    return{
                      ...state,
                      invoice:"something wrong"
                    }
                  }
                  case CHANGE_DATA_INVOICE:{
                    const invoice=state.dataInvoice;
                    invoice[action.name]=action.value;
                    return{
                      ...state,
                      dataInvoice:invoice
                    }
                  }
                  case ADD_ROW_PRODUCT:{
                  const product=state.dataInvoice;
                    product.products.push({
                    id_product : '',
                     name : '',
                    date :new Date(),
                    quatity : 1,
                    unit : 0,
                   priceUnit : 0,
                   vat: 0,
                   price : 0,
                   priceHT : 0
                  });
                  console.log(product);
                  return{
                    ...state,
                    dataInvoice:product
                  }
               
              }
                  case CHANGE_DATA_INVOICE_PRODUCTS:{
                    console.log(action.products);
                    const {name,value,index,products}=action;
                    console.log(state.dataInvoice.priceTTC);
                   const invoice=state.dataInvoice;
                   let product=invoice.products[index];
                      
                  
                  if(name==="name"){
                   product.name=value;
                    products.map((prod)=>{
                     console.log(prod);
                      if(prod.name===value){
                    product.priceUnit=prod.priceWT;
                  product.unit=prod.unit;
                    product.vat=prod.vat;       
                    product.price=prod.priceWT;
                    product.priceHT=prod.priceTTC;

                  }
                })
              }
                   if(name==="quatity"){
                    product.quatity=value;
                   }
                   if(product.quatity>0){
                   product.priceHT=product.priceHT*product.quatity;
                   }
                   invoice.totalTTC=0;
                   invoice.net=0;
                   invoice.products.map((prod)=>{
                    invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                    invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                   })
                   console.log(invoice.totalTTC);
                   return{
                    ...state,
                    dataInvoice:invoice
                  }
                  }
                  case UPDATE_INVOICE:{
                    const newInvoices=state.invoices;
                    const index=newInvoices.indexOf(state.detailInvoice);
                    console.log(index);
                    console.log(action.invoice);
                    const invoice=action.invoice;
                    newInvoices[index]={...invoice};
                    console.log(newInvoices);
                    return{ 
                      ...state,
                      invoice:'',
                      detailInvoice:'',
                    invoices:newInvoices
                    }
                  }
                  case ADD_INVOICE:{
                   
                    state.invoices.push(action.invoice);
                    const newInvoices=state.invoices;
                    return{
                      ...state,
                      data:{
                        companyName: '',
                        billNumber : 0,
                        billDate : '',
                        conditionPayment : 0,
                        deadline :'',
                        message : '',
                        notesFooter : '',
                        discount : 0,
                        totalTTC : 0,
                        products : [ 
                            {
                                id_product : '',
                                name : '',
                                date :'',
                                quatity : 0,
                                unit : 0,
                                priceUnit : 0,
                                vat: 0,
                                price : 0,
                                priceHT : 0
                            }    
                        ] 
                     },
                     invoices:newInvoices,
                     invoice:''
                    }
                  }
                  case CHANGE_VALUE_INVOICE:{
                    const invoice=state.detailInvoice;
                    invoice[action.name]=action.value;
                    return{
                      ...state,
                      detailInvoice:invoice
                    }
                  };
                  case DELETE_INVOICE:{
                    const newInvoices=state.invoices.filter(product=>product._id!==action.id);
                    console.log(newInvoices);
                  return{
                    ...state,
                    invoices:newInvoices,
                    detailInvoice:''
                  }
                  }
                  default:{
                    return state
                }
     }
    }