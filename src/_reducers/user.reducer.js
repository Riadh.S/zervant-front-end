import { REGISTER_SUCCESS,REGISTER_FAILED} from "../_actions/types"

const initialState ={
  token : '',
  auth  :'',
  userResponse:'',
  loggingIn:false
  }

export function users(state = initialState, action) {
  switch (action.type) {
    case REGISTER_SUCCESS:{
      return {
        loggingIn: true,
        auth: action.auth,
        token: action.token
      }
    }
    case REGISTER_FAILED:{
      return {
        ...state,
        userResponse:action.message
      }
    }
    default:
      return state
  }
}