import _ from "lodash";
import {
  FETECHED_ALL_CUSTOMERS,
  ADD_CUSTOMER,
  ORDER_CUSTOMERS,
  CHANGE_DATA_CUSTOMER,
  SEARCH_CUSTOMERS,
  DELETE_CUSTOMER,
  DETAIL_CUSTOMERS,
  PAGINATION_CUSTOMER,
  UPDATE_CUSTOMER,
  UPDATE_CUSTOMER_FAILED,
  CHANGE_VALUE_CUSTOMER,
  CHANGE_CHECKBOX_DATA_CUSTOMER
} from "../_actions/types";
const INITIAL_STATE = {
  customersList: [],
  searchData: "",
  detailCustomer: "",
  listPage: 4,
  currentPage: 1,
  data: {
    deliveryMethod: { valuePD: 0, displayPD: false },
    TypeOfCustomer: ["New company customer", "New private customer"],
    CompanyName: "",
    CompanyRegistrationNumber: "",
    VatNumber: "",
    title: "",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    mobilePhone: "",
    address: "",
    postCode: "",
    city: "",
    country: ["Tunisia", "France", "Deutch"],
    invoicingLanguage: ["french", "English"],
    paymentTerm: ["30", "60", "90"],
    eInvoiceAddress: "",
    eInvoiceOperator: ["4 solutions", "3E consultancy solutions"],
    eInvoiceNetworkType: ["Public PEPPOL network", "Private operator network"]
  },
  customer: ""
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETECHED_ALL_CUSTOMERS:
      return {
        ...state,
        customersList: action.payload
      };
    case ORDER_CUSTOMERS: {
      const customers = _.orderBy(state.customers, action.valeur, "asc");
      console.log(customers);
      return {
        ...state,
        customers: action.payload
      };
    }
    case SEARCH_CUSTOMERS: {
      return {
        ...state,
        searchData: action.payload
      };
    }
    case DETAIL_CUSTOMERS: {
      return {
        ...state,
        detailCustomer: action.payload
      };
    }
    case PAGINATION_CUSTOMER: {
      console.log(action.nbrPage);
      return {
        ...state,
        currentPage: action.payload
      };
    }
    case UPDATE_CUSTOMER_FAILED: {
      return {
        ...state,
        customer: "update customer failed"
      };
    }
    case CHANGE_DATA_CUSTOMER: {
      const customer = state.data;
      console.log("state.data", state.data);
      customer[action.payload.name] = action.payload.value;
      return {
        ...state,
        data: customer
      };
    }
    case UPDATE_CUSTOMER: {
      const newCustomers = state.customers;
      const index = newCustomers.indexOf(state.detailCustomer);
      console.log(index);
      console.log(action.payload);
      const customer = action.payload;
      newCustomers[index] = { ...customer };
      console.log(newCustomers());
      return {
        ...state,
        customer: "",
        detailCustomer: "",
        customers: newCustomers
      };
    }
    case ADD_CUSTOMER: {
      state.customers.push(action.payload);
      const newCustomers = state.customers;
      return {
        ...state,
        data: {
          TypeOfCustomer: ["New company customer", "New private customer"],
          CompanyName: "",
          CompanyRegistreationNumber: "",
          VatNumber: "",
          title: "",
          firstName: "",
          lastName: "",
          email: "",
          phone: "",
          mobilePhone: "",
          address: "",
          postCode: "",
          city: "",
          country: ["Tunisia", "France", "Deutch"],
          invoicingLanguage: ["french", "English"],
          paymentTerm: ["30", "60", "90"],
          eInvoiceAddress: "",
          eInvoiceOperator: ["4 solutions", "3E consultancy solutions"],
          eInvoiceNetworkType: [
            "Public PEPPOL network",
            "Private operator network"
          ]
        },
        customers: newCustomers
      };
    }
    case CHANGE_VALUE_CUSTOMER: {
      const customer = state.detailCustomer;
      customer[action.payload] = action.value;

      return {
        ...state,
        detailCustomer: action.payload
      };
    }
    case DELETE_CUSTOMER: {
      const newCustomers = state.customers.filter(
        customer => customer._id !== action._id
      );
      console.log(newCustomers);
      return {
        ...state,
        customers: newCustomers
      };
    }

    case CHANGE_CHECKBOX_DATA_CUSTOMER: {
      //console.log(action.name, action.value, "checkbox customer reducer");
      const customer = state.data;
      // let info = customer;
      // if (action.payload.name === "displayPD") {
      //   info = customer.paymentCondition;
      // } else if (action.name === "displayDI") {
      //   info = customer.delayInterest;
      // }

      customer[action.payload.name] = !action.payload.value;
      console.log("customer[action.payload.name]", customer);
      return {
        ...state,
        data: customer
      };
    }

    default:
      return state;
  }
};
