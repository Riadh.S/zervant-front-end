import {FETECHED_ALL_PRODUCT,PRODUCT_START,ADD_PRODUCT,DELETE_PRODUCT,CHANGE_DATA_PRODUCT,ORDER_PRODUCTS,SEARCH_PRODUCTS,DETAIL_PRODUCTS,PAGINATION_PRODUCT,UPDATE_PRODUCT_FAILED,UPDATE_PRODUCT,CHANGE_VALUE_PRODUCT} from '../_actions/types';
import _ from 'lodash';
const initialeState={
        searchData:'',
        detailProduct:'',
        listPage:4,
     currentPage:1,
     data:{
      name:'',
      description:'',
      unit:'',
      priceBasedOn:'',
      priceWT:0,
      vat:0,
      priceTTC:0
   },
   product:'',
   load:false,
   products:[]
  }
export function product(state = initialeState, action) {
                switch (action.type) {
                  case PRODUCT_START:{
                    return{
                    ...state,
                    load:true
                    }
                  }
                  case FETECHED_ALL_PRODUCT:{
                    console.log(action.products);
                    return {
                      ...state,
                      currentPage:1,
                      products:action.products,
                      load:false,
                      searchData:'',
                      detailProduct:''
                    }
                    }
                    case ORDER_PRODUCTS:{
                      let products=[];
                      if(action.order===1){
                      products=_.orderBy(state.products,action.valeur,"asc");
                      }
                      if(action.order===2){
                        products=_.orderBy(state.products,action.valeur,"desc");
                        }
                      console.log(products);
                      return{
                        ...state,
                        products:products
                      }
                    }
                    case SEARCH_PRODUCTS:{
                      return{
                        ...state,
                        searchData:action.name,
                        length:state.products.length
                      }
                    };
                    case DETAIL_PRODUCTS:{
                      return{
                        ...state,
                        detailProduct:action.product
                       
                      }
                    }
                    case PAGINATION_PRODUCT:{
                      console.log(action.nbrPage);
                      return {
                      ...state,
                      currentPage:action.nbrPage
                    
                      
                    }
                  }
                  case UPDATE_PRODUCT_FAILED:{
                    return{
                      ...state,
                      product:"something wrong"
                      
                    }
                  }
                  case CHANGE_DATA_PRODUCT:{
                    const product=state.data;
                    product[action.name]=action.value;
                    return{
                      ...state,
                      data:product
                      
                    }
                  }
                  case UPDATE_PRODUCT:{
                    const newProducts=state.products;
                    const index=newProducts.indexOf(state.detailProduct);
                    console.log(index);
                    console.log(action.product);
                    const product=action.product;
                    newProducts[index]={...product};
                    console.log(newProducts);
                    return{ 
                      ...state,
                      product:'',
                      detailProduct:'',
                    products:newProducts
                   
                    }
                  }
                  case ADD_PRODUCT:{
                   
                    state.products.push(action.product);
                    const newProducts=state.products;
                    return{
                      ...state,
                      data:{
                        name:'',
                        description:'',
                        unit:'',
                        priceBasedOn:'',
                        priceWT:0,
                        vat:0,
                        priceTTC:0
                     },
                      products:newProducts,
                  product:''
                    }
                  }
                  case CHANGE_VALUE_PRODUCT:{
                    const product=state.detailProduct;
                    product[action.name]=action.value;
                    return{
                      ...state,
                      detailProduct:product
                    
                    }
                  }
                  case DELETE_PRODUCT:{
                    const newProducts=state.products.filter(product=>product._id!==action.id);
                    console.log(newProducts);
                  return{
                    ...state,
                    products:newProducts,
                    detailProduct:''
                  }
                  };
                  default:{
                    return state
                }
     }
    }