
import { BILLSETTING_INFORMATION_FOUND,BILLSETTING_INFORMATION_NOTFOUND,ADD_BILLSETTING,UPDATE_BILLSETTING,CHANGE_DATA_BILLSETTING,CHANGE_CHECKBOX_DATA_BILLSETTING} from "../_actions/types";
const initialState={
    data:{
        currency:'EUR',
        language:'English' ,
        billHeader:'',
        billType:'',
        paymentCondition:{valuePD:0,displayPD:false},
        delayInterest:{valueDI:0,displayDI:false},
        paymentRef:false,
        attentionOf:false,
        displayColunms:[
          {label:'date',value : true},
         { label:'quatity',value : true},
          {label:'unit',value : true},
          { label:'unitPrice',value : true},
           {label:'vat',value : true},
          {label:'totalVat',value : true},
          {label:'priceTTC',value : true}
         ] ,
        discountSetting:[
          {label:'Total',value  : true},
          {label:'withRow' ,value : true},
          {label:'price' ,value : true},
          {label:'percent' ,value : true},
          {label:'BaseHT' ,value : true},
         {label:'BaseTTC' ,value : true}
        ],
        message:'',
        notesFooter:''    
    },
    found:false
}
export function billSetting(state=initialState,action){
    switch (action.type){
        case  BILLSETTING_INFORMATION_FOUND:{
          console.log(action.data);  
          return{
               data:action.data,
               found:true 
            }
        }
        case BILLSETTING_INFORMATION_NOTFOUND:{
            return{
              data:{
                currency:'',
                language:'' ,
                billHeader:'',
                billType:'',
                paymentCondition:{valuePD:0,displayPD:false},
                delayInterest:{valueDI:0,displayDI:false},
                paymentRef:false,
                attentionOf:false,
                displayColunms:[
                  {label:'name',value : true},
                 { label:'description',value : true},
                  {label:'unit',value : true},
                  { label:'priceBasedOn',value : true},
                   {label:'priceWT',value : true},
                  {label:'vat',value : true},
                  {label:'priceTTC',value : true}
                 ] ,
                discountSetting:[
                  {label:'Total',value  : true},
                  {label:'withRow' ,value : true},
                  {label:'price' ,value : true},
                  {label:'percent' ,value : true},
                  {label:'BaseHT' ,value : true},
                 {label:'BaseTTC' ,value : true}
                ],
                message:'',
                notesFooter:''    
            },
            found:false 
            }
        }
    
    case CHANGE_DATA_BILLSETTING:{
        const bill=state.data;
        if(action.name==="valueDI"){
        bill.delayInterest[action.name]=action.value;
        } else if(action.name==="valuePD"){
            bill.paymentCondition[action.name]=action.value;
        }
        else{
        bill[action.name]=action.value;
        }
        return{
          ...state,
          data:bill
        }
      }
      case CHANGE_CHECKBOX_DATA_BILLSETTING:{
        console.log(action.name,action.value);
        const bill=state.data;
       let info=bill;
        if(action.name==="displayPD"){
        info=bill.paymentCondition;
       }else if(action.name==="displayDI"){
       info=bill.delayInterest;
       }
        if(action.value===true){
        info[action.name]=false;
      }else{
        info[action.name]=true;
      }
      
      return{
          ...state,
          data:bill
      }
      }
      case ADD_BILLSETTING:{
          return {
              ...state,
              found:true
      }
    }
    case UPDATE_BILLSETTING:{
   return state;
    }
    default:{
      return state;
}
}
}