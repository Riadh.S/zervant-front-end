import { combineReducers } from "redux";
import { authentication } from "./auth.reducer";
import { users } from "./user.reducer";
import { product } from "./product.reducer";
import { companyInformation } from "./companyInformation.reducer";
import { payment } from "./payment.reducer";
import { userAccount } from "./userAccount.reducer";
import { billSetting } from "./billSetting.reducer";
import { devisSetting } from "./devisSetting.reducer";
import { invoice } from "./invoice.reducer";
import CustomerReducer from "./CustomerReducer";
const rootReducer = combineReducers({
  authentication,
  users,
  product,
  companyInformation,
  payment,
  userAccount,
  billSetting,
  devisSetting,
  invoice,
  customers: CustomerReducer
});
export default rootReducer;
