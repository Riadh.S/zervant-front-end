import {USER_INFORMATION} from '../_actions/types';
const initialeState={
    data:{
        name:'',
        email:'' ,
        country :'',
        password:'',
        companyName:'',
        currency:'',
        language:''
    }
}
export function userAccount(state=initialeState,action){
    switch (action.type){
        case USER_INFORMATION:{
            return{
                data:action.user
            }
        }
    default:{
        return state;
    }
    }
  
}