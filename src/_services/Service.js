import axios from 'axios';
import {baseUrl} from '../config/config';
import { history } from '../_helpers';

export const Service = {
    get,
    post,
    put,
    deleteDetail,
    getpdf
};

async function get(apiEndpoint){
    try{
    const response=await axios.get(baseUrl+apiEndpoint, getOptions());   
    console.log(response);
    return response;
    
}
    catch(err){
        
       if(err.message==="Request failed with status code 401"){
            localStorage.removeItem('auth');
            localStorage.removeItem('token');
            history.push('/login');
        }
        console.log("Error in response");
        console.log(err);
    }
}

 async function post(apiEndpoint, payload){
    try{
    const response=await axios.post(baseUrl+apiEndpoint, payload, getOptions());  
       
    return response;
    
 }catch(err){
    
console.log(err); 
if(err.message==="Request failed with status code 401"){
    localStorage.removeItem('auth');
    localStorage.removeItem('token');
    history.push('/login');
}
}
 }  
async function put(apiEndpoint, payload){
    try{
    const response=await axios.put(baseUrl+apiEndpoint, payload, getOptions());
    
    return response;
    
    }
    catch(err){
       
        console.log(err);
        if(err.message==="Request failed with status code 401"){
            localStorage.removeItem('auth');
            localStorage.removeItem('token');
            history.push('/login');
        }
    }
}

async function deleteDetail(apiEndpoint){
    try{
    const response=await axios.delete(baseUrl+apiEndpoint, getOptions());
       
    return response;
    }
    catch(err){
        
        console.log(err);
        if(err.message==="Request failed with status code 401"){
            localStorage.removeItem('auth');
            localStorage.removeItem('token');
            history.push('/login');
        }
    }
}
async function getpdf(){
    try{
        let apiEndpoint='bills/fetch-pdf';
    const response=await axios.get(baseUrl+apiEndpoint,getOptions(),{ responseType: 'blob' });   
    console.log(response);
    return response;
    
}
    catch(err){
        
        console.log("Error in response");
        console.log(err);
    }
}

function getOptions(){
    let options = {}; 
    if(localStorage.getItem('token')){
        options.headers = { 'x-access-token': localStorage.getItem('token') };
    }
    return options;
}