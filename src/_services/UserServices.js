import React, { Component } from 'react';
import Axios from 'axios';
import jwtDecode from 'jwt-decode';
const headers={
  'x-access-token': localStorage.getItem('token')
}
export function getCurrentUser(){
    try{
        const jwt=localStorage.getItem("token");
        const resultat=jwtDecode(jwt);
        const email=resultat.email;
        const indexAt=email.indexOf("@");
        const user=email.slice(0,indexAt); 
        return user;
      }catch(err){
        return null;
      }
}
export function getIdUser(){
  try{
      const jwt=localStorage.getItem("token");
      const resultat=jwtDecode(jwt);
      return resultat.user_id;
    }catch(err){
      return null;
    }
}
export async function getUser(id){
  try{
    const response=await Axios.get(`http://127.0.0.1:8001/api/users/${id}`,{headers});
    if(response.status===200){
      return response.data.data;
    }
  }
  catch(err){
    console.log(err);
    return null;
  }
}