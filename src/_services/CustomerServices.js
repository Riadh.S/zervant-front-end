import Axios from "axios";
const headers = {
  "x-access-token": localStorage.getItem("token")
};
export async function getCustomers(id, headers) {
  const response = await Axios.get(
    `http://127.0.0.1:8001/api/customers/${id}`,
    { headers }
  );
  console.log(response);
  if (response.status === 200) {
    console.log(response.data.data);
    return response.data.data;
  } else {
    return [];
  }
}
export async function deleteCustomer(id) {
  const result = await Axios.delete(
    `http://127.0.0.1:8001/api/customers/${id}`,
    {
      headers
    }
  );
  if (result.status === 200) {
    return true;
  } else {
    return false;
  }
}
export async function addCustomer(customer) {
  const result = await Axios.post(
    `http://127.0.0.1:8001/api/customers`,
    { headers },
    { customer }
  );
  if (result.status === 200) {
    return true;
  } else {
    return false;
  }
}
export async function getCustomer(id) {
  const result = await Axios.post(`http://127.0.0.1:8001/api/customers/${id}`, {
    headers
  });
  if (result.status === 200) {
    return result.data.data;
  } else {
    console.log(result);
  }
}
export async function putCustomer(id, customer) {
  const result = await Axios.put(
    `http://127.0.0.1:8001/api/customers/${id}`,
    { headers },
    { customer }
  );
  if (result.status === 200) {
    return true;
  } else {
    return false;
  }
}
